package de.roxhit.marioparty.core;

import de.roxhit.marioparty.core.utils.SequencedRunnable;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by jonio on 08.07.2017.
 */
public class Sounds {




    public static void playSound(Player p, Plugin pl){
        playSound(p, pl, Sound.BLOCK_NOTE_HARP);
    }



    public static void playSound(Player p, Plugin pl, Sound sound){

        new SequencedRunnable(pl){
            @Override
            public void run() {
                synced(() -> {
                    p.playSound(p.getLocation(), sound, 1, 0.8f);
                    p.playSound(p.getLocation(), sound, 0.2f, 0.8f);
                });

                wait(250);

                synced(() -> {
                    p.playSound(p.getLocation(), sound, 1, 0.64f);
                    p.playSound(p.getLocation(), sound, 0.2f, 0.64f);
                });

                wait(150);

                synced(() -> {
                    p.playSound(p.getLocation(), sound, 1, 0.58f);
                    p.playSound(p.getLocation(), sound, 0.2f, 0.58f);
                });

                wait(150);

                synced(() -> {
                    p.playSound(p.getLocation(), sound, 1, 0.7f);
                    p.playSound(p.getLocation(), sound, 0.2f, 0.7f);
                });

                wait(250);

                synced(() -> {
                    p.playSound(p.getLocation(), sound, 1, 0.9f);
                    p.playSound(p.getLocation(), sound, 0.2f, 0.9f);
                });

                wait(250);

                synced(() -> {
                    p.playSound(p.getLocation(), sound, 1, 0.79f);
                    p.playSound(p.getLocation(), sound, 0.2f, 0.79f);
                });

                super.run();
            }
        }.runTask();

    }
}
