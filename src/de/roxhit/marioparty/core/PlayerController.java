package de.roxhit.marioparty.core;

import de.roxhit.Vector.Vectors;
import de.roxhit.marioparty.core.events.PlayerControllerMovementFinished;
import de.roxhit.marioparty.core.utils.Promise;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;

public class PlayerController {


    private Plugin pl;
    private GamePlayer p;
    private GameMap map;

    private int curField = -1;

    private ArrayList<Anchor> curAnchors = new ArrayList<>();
    private int counter = 0;
    private boolean finished = false;

    public boolean isMovingByController = false;

    public PlayerController(Plugin pl, GamePlayer p, GameMap map) {
        this.pl = pl;
        this.p = p;
        this.map = map;
    }


    public Promise moveAlong(ArrayList<Anchor> anchors){
        isMovingByController = true;

        return new Promise(){
            @Override
            public void run() {

                PlayerController.this.curAnchors = anchors;
                PlayerController.this.counter = 0;
                PlayerController.this.finished = false;

                p.setMoveEnabled(false);

                nextAnchor();

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if(finished){
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    Bukkit.getServer().getPluginManager().callEvent(new PlayerControllerMovementFinished(p, curField));
                                    p.setMoveEnabled(true);
                                }
                            }.runTask(pl);
                            isMovingByController = false;
                            finish();
                            cancel();
                        }
                    }
                }.runTaskTimerAsynchronously(pl, 0, 5);

//                new BukkitRunnable() {
//                    @Override
//                    public void run() {
//                        for(Anchor anchor : anchors){
//
//                            moveTo(anchor).then(runnable::notify);
//
//                            synchronized (runnable){
//                                try {
//                                    runnable.wait();
//                                } catch (InterruptedException e) {
//                                    e.printStackTrace();
//                                }
//                                System.out.println("Reached!");
//
//                                new BukkitRunnable() {
//                                    @Override
//                                    public void run() {
//                                        Bukkit.getPlayer("walamana").sendMessage("Reached");
//                                    }
//                                }.runTask(pl);
//                            }
//                        }
//
//                        finish();
//                    }
//                }.runTaskAsynchronously(pl);

            }
        };
    }

    public void nextAnchor(){


        if(curAnchors.size() == counter){
            finished = true;
            return;
        }

        Anchor anchor = curAnchors.get(counter);

        final double x = anchor.getX();
        final double z = anchor.getZ();


        BukkitTask timeout = new BukkitRunnable() {
            @Override
            public void run() {
                System.out.println("Player to slow, teleporting to next point");

                p.getPlayer().teleport(anchor);
                cancel();
            }
        }.runTaskLater(pl, 20*5);

        new BukkitRunnable() {
            @Override
            public void run() {
                final double px = p.getPlayer().getLocation().getX();
                final double pz = p.getPlayer().getLocation().getZ();
                if(Math.abs((x - px)) >= 1.5 || Math.abs((z - pz)) >= 1.5){
                    p.getPlayer().setVelocity(Vectors.VectorPoint(p.getPlayer(), anchor).multiply( anchor.multiply).setY(anchor.addY).normalize());
                }else{

                    if(!anchor.isSecondaryAnchor()){
                        curField++;

                        if(curField >= MarioPartyAPI.manager.map.fields.size()){
                            curField = 0;
                        }

                        p.getPlayer().playSound(p.getPlayer().getLocation(), Sound.BLOCK_NOTE_PLING, 1, 1);
                    }

//                    p.field++;
//                    if(p.field >= MarioPartyAPI.manager.map.fields.size()){
//                        p.field = 1;
//                    }
                    timeout.cancel();
                    cancel();

                    p.getScoreboardManager().updateScoreboard();

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            counter++;
                            nextAnchor();
                        }
                    }.runTaskLater(pl, 4);
                }
            }
        }.runTaskTimer(pl, 0, 5);

    }

    public int getCurField() {
        return curField;
    }
}
