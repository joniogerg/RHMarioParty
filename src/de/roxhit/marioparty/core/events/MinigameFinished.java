package de.roxhit.marioparty.core.events;

import de.roxhit.marioparty.core.interfaces.Minigame;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class MinigameFinished extends Event{
    private static final HandlerList handlers = new HandlerList();
    private Minigame minigame;

    public MinigameFinished(Minigame minigame) {
        super();
        this.minigame = minigame;
    }

    public Minigame getMinigame() {
        return minigame;
    }


    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
