package de.roxhit.marioparty.core.setup;

import de.roxhit.marioparty.core.GameMap;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;

public class MapSetupManager {


    private Plugin pl;

    public GameMap toSetup;

    private HashMap<Player, MapSetup> setupMode = new HashMap<>();

    public MapSetupManager(Plugin pl) {
        this.pl = pl;

    }




    /**
     * Sets the players setup mode state
     * @param p The Player to change the state
     * @param state The state to be set
     */

    public void setPlayerSetupMode(Player p, boolean state){
        if(toSetup == null){
            p.sendMessage("§4Select a map to set up first!");
            return;
        }
        if(state && !isInSetupMode(p)){
            setupMode.put(p, new MapSetup(p, pl, this));
        }else if(!state && isInSetupMode(p)){
            setupMode.get(p).removeAllArmorStands();
            setupMode.remove(p);
        }
    }

    public void removeArmorStands(){
        for(MapSetup s : setupMode.values()){
            s.removeAllArmorStands();
        }
    }


    public boolean isInSetupMode(Player p){
        return setupMode.containsKey(p);
    }
}
