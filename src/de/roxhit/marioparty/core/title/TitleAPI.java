package de.roxhit.marioparty.core.title;

import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutTitle;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class TitleAPI {

    public static void sendTitlesPacket(Player p, String msg, Styles styles, int fadeIn, int duration, int fadeOut, PacketPlayOutTitle.EnumTitleAction type){
        IChatBaseComponent chatTitle = IChatBaseComponent.ChatSerializer.a(
                "{\"text\": \"" + msg + "\"," +
                    "\"color\": \"" + styles.getColor().name().toLowerCase() + "\"," +
                    "\"bold\": " + styles.isBold() + "," +
                    "\"italic\": " + styles.isItalic() + "," +
                    "\"underlined\": " + styles.isUnderlined() + "," +
                    "\"strikethrough\": " + styles.isStrikethrough() + "," +
                    "\"obfuscated\": " + styles.isObfuscated() + "}");

        PacketPlayOutTitle title = new PacketPlayOutTitle(type, chatTitle);
        PacketPlayOutTitle length = new PacketPlayOutTitle(fadeIn, duration, fadeOut);


        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(length);
    }

    public static void sendTitle(Player p, String msg, Styles styles, int fadeIn, int duration, int fadeOut){
        sendTitlesPacket(p, msg, styles, fadeIn, duration, fadeOut, PacketPlayOutTitle.EnumTitleAction.TITLE);
    }

    public static void sendTitle(Player p, String msg, Styles styles,  int duration){
        sendTitle(p, msg, styles, 5, duration, 5);
    }

    public static void sendTitle(Player p, String msg, Styles styles){
        sendTitle(p, msg, styles, 20);
    }

    public static void sendSubtitle(Player p, String msg, Styles styles, int fadeIn, int duration, int fadeOut){
        sendTitlesPacket(p, msg, styles, fadeIn, duration, fadeOut, PacketPlayOutTitle.EnumTitleAction.SUBTITLE);
    }

    public static void sendSubtitle(Player p, String msg, Styles styles, int duration){
        sendSubtitle(p, msg, styles, 5, duration, 5);
    }

    public static void sendSubtitle(Player p, String msg, Styles styles){
        sendSubtitle(p, msg, styles, 20);
    }
}
