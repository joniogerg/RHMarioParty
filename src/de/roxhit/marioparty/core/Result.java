package de.roxhit.marioparty.core;

import com.mojang.authlib.GameProfile;
import net.minecraft.server.v1_12_R1.GameProfileBanList;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jonio on 02.07.2017.
 */
public abstract class Result {

    private HashMap<Integer, GamePlayer> results = new HashMap<>();

    public Result() {
    }

    public void addPlayer(GamePlayer p){
        if(p == null)return;
        results.put(results.size() + 1, p);
        onResultChange();
    }

    public abstract void onResultChange();

    public void setPlace(GamePlayer p, Integer place){
        results.put(place, p);
        onResultChange();
    }

    public GamePlayer getPlayer(int place){
        return results.get(place);
    }

    public Integer getPlace(GamePlayer player){
        for(Map.Entry<Integer, GamePlayer> e : results.entrySet()){
            if(e.getValue().getPlayer().getName().equalsIgnoreCase(player.getPlayer().getName()))
                return e.getKey();
        }
        return -1;
    }

    public HashMap<Integer, GamePlayer> getResults() {
        return results;
    }
}
