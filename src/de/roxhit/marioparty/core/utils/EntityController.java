package de.roxhit.marioparty.core.utils;

import de.roxhit.Vector.Vectors;
import de.roxhit.marioparty.core.Anchor;
import de.roxhit.marioparty.core.GameMap;
import de.roxhit.marioparty.core.GamePlayer;
import de.roxhit.marioparty.core.MarioPartyAPI;
import de.roxhit.marioparty.core.events.PlayerControllerMovementFinished;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class EntityController {


    private Plugin pl;
    private Entity p;
    private GameMap map;

    private int curField = -1;

    private ArrayList<Anchor> curAnchors = new ArrayList<>();
    private int counter = 0;
    private boolean finished = false;

    public boolean isMovingByController = false;

    public EntityController(Plugin pl, Entity p, GameMap map) {
        this.pl = pl;
        this.p = p;
        this.map = map;
    }


    public Promise moveAlong(ArrayList<Anchor> anchors){
        isMovingByController = true;

        return new Promise(){
            @Override
            public void run() {

                EntityController.this.curAnchors = anchors;
                EntityController.this.counter = 0;
                EntityController.this.finished = false;

                nextAnchor();

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if(finished){
                            isMovingByController = false;
                            finish();
                            cancel();
                        }
                    }
                }.runTaskTimerAsynchronously(pl, 0, 5);

//                new BukkitRunnable() {
//                    @Override
//                    public void run() {
//                        for(Anchor anchor : anchors){
//
//                            moveTo(anchor).then(runnable::notify);
//
//                            synchronized (runnable){
//                                try {
//                                    runnable.wait();
//                                } catch (InterruptedException e) {
//                                    e.printStackTrace();
//                                }
//                                System.out.println("Reached!");
//
//                                new BukkitRunnable() {
//                                    @Override
//                                    public void run() {
//                                        Bukkit.getPlayer("walamana").sendMessage("Reached");
//                                    }
//                                }.runTask(pl);
//                            }
//                        }
//
//                        finish();
//                    }
//                }.runTaskAsynchronously(pl);

            }
        };
    }

    public void nextAnchor(){


        if(curAnchors.size() == counter){
            finished = true;
            return;
        }

        Anchor anchor = curAnchors.get(counter);

        final double x = anchor.getX();
        final double z = anchor.getZ();


        BukkitTask timeout = new BukkitRunnable() {
            @Override
            public void run() {
                System.out.println("Player to slow, teleporting to next point");

                p.teleport(anchor);
                cancel();
            }
        }.runTaskLater(pl, 20*5);

        new BukkitRunnable() {
            @Override
            public void run() {
                final double px = p.getLocation().getX();
                final double pz = p.getLocation().getZ();
                if(Math.abs((x - px)) >= 1.5 || Math.abs((z - pz)) >= 1.5){
                    p.setVelocity(
                            Vectors.VectorPoint(p, anchor)
                                    .multiply( anchor.multiply)
                                    .setY(anchor.addY)
                                    .normalize());

                    Vector vector = new Vector();
                }else{

                    if(!anchor.isSecondaryAnchor()){
                        curField++;

                        if(curField >= MarioPartyAPI.manager.map.fields.size()){
                            curField = 0;
                        }

                    }

//                    p.field++;
//                    if(p.field >= MarioPartyAPI.manager.map.fields.size()){
//                        p.field = 1;
//                    }
                    timeout.cancel();
                    cancel();

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            counter++;
                            nextAnchor();
                        }
                    }.runTaskLater(pl, 4);
                }
            }
        }.runTaskTimer(pl, 0, 5);

    }

    public int getCurField() {
        return curField;
    }
}
