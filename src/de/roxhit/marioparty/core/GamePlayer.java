package de.roxhit.marioparty.core;

import de.roxhit.Vector.Vectors;
import de.roxhit.heads.main.CustomHead;
import de.roxhit.marioparty.core.events.PlayerHitDice;
import de.roxhit.marioparty.core.interfaces.MapModule;
import de.roxhit.marioparty.core.interfaces.Minigame;
import de.roxhit.marioparty.core.utils.Promise;
import de.roxhit.marioparty.core.utils.SequencedRunnable;
import de.walamana.fb64s.main.Category;
import org.bukkit.*;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class GamePlayer implements Listener {

    private Player p;
    private Plugin pl;

    @Deprecated
    private boolean moveEnabled = true;
    public PlayerController controller;

    private boolean diceRolling = false;
    private ArmorStand dice = null;
    private BukkitRunnable diceTask = null;
    private int diceNumber = 1;
    private BukkitRunnable timeoutTask = null;

    private BukkitRunnable powerUpTask = null;
    private boolean openingPowerup = false;

    private PlayerScoreboardManager scoreboardManager;

    public boolean inAction = false;
    public boolean inModule = false;


    public static int instances = 0;


    public GamePlayer(Player p, Plugin pl) {
        this.p = p;
        this.pl = pl;

        pl.getServer().getPluginManager().registerEvents(this, pl);
        instances++;
        System.out.println("Instances " + instances);

        this.controller = new PlayerController(pl, this, MarioPartyAPI.manager.map);
        this.scoreboardManager = new PlayerScoreboardManager(this);

        scoreboardManager.updateScoreboard();

        p.sendMessage( "§7Du kannst die Musik mit /music starten bzw. stoppen " +
                "\n§7WEnn du das offizielle Resourcepack nutzen möchtest, nutze den Befehl /resourcepack");
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e){
        Player p = e.getPlayer();
        if(p.getName().equalsIgnoreCase(this.p.getName())){
            if(diceIsRolling() && !dice.isDead()){
                Location frontLocation = p.getEyeLocation().add(p.getLocation().getDirection().multiply(2)).add(0, -1.6, 0);
                float yawChange =  e.getTo().getYaw() - e.getFrom().getYaw();
                frontLocation.setYaw(frontLocation.getYaw() + yawChange - 180);
                dice.teleport(frontLocation);
            }
            if(!inModule && !controller.isMovingByController  && !MarioPartyAPI.manager.minigameManager.minigameIsRunning && !MarioPartyAPI.manager.isRunning()){
                if(controller.getCurField() == 0 || controller.getCurField() >= MarioPartyAPI.manager.map.fields.size())return;
                if(controller.getCurField() == -1){
                    return;
                }
                Location fieldLocation = MarioPartyAPI.manager.map.getField(controller.getCurField()).getFinalAnchor();
                if(fieldLocation.distance(p.getLocation()) >= 1.5){
                    if(MarioPartyAPI.manager.minigameManager.minigameIsRunning)
                        return;
                    p.setVelocity(Vectors.VectorPoint(p, fieldLocation).setY(-1).multiply(0.1));
                }
            }
            if(controller.isMovingByController){
                p.getWorld().spawnParticle(Particle.NOTE, p.getLocation(), 10);
            }

        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        Player p = e.getPlayer();
        if(p.getName().equalsIgnoreCase(this.p.getName())){
            e.setCancelled(true);
            if(diceIsRolling()){
                stopDice();
            }
        }
    }
    @EventHandler
    public void onEntityInteract(PlayerInteractAtEntityEvent e){
        e.setCancelled(true);
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onDamage(EntityDamageEvent e){
        if(e.getEntity() instanceof Player && e.getEntity().getName().equalsIgnoreCase(p.getName())){
            if(!MarioPartyAPI.manager.minigameManager.minigameIsRunning){
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        if(e.getPlayer().getName().equalsIgnoreCase(p.getName())){
            unregister();
            e.setQuitMessage(MarioPartyAPI.PREFIX_SYSTEM  + p.getName() + " hat das Spiel verlassen!");
        }
    }
    @EventHandler
    public void onSprint(PlayerToggleSprintEvent e){
        if(e.getPlayer().getName().equalsIgnoreCase(p.getName())){
            if(!isMoveEnabled()){
                e.setCancelled(true);
                e.getPlayer().setSprinting(false);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDrop(PlayerDropItemEvent e){
        if(e.getPlayer().getName().equalsIgnoreCase(p.getName())){
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onSwap(PlayerSwapHandItemsEvent e){
        if(e.getPlayer().getName().equalsIgnoreCase(p.getName())){
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventory(InventoryClickEvent e){
        if(e.getWhoClicked().getName().equalsIgnoreCase(p.getName())){
            e.setCancelled(true);
        }
    }




    @EventHandler
    public void onInvClick(InventoryInteractEvent e){
        System.out.println(e.getWhoClicked().getName() + ", " + p.getName());
        if(e.getWhoClicked().getName().equalsIgnoreCase(p.getName())){
                e.setCancelled(true);
        }
    }




    public void unregister(){
        InventoryInteractEvent.getHandlerList().unregister(this);
        PlayerMoveEvent.getHandlerList().unregister(this);
        PlayerInteractEvent.getHandlerList().unregister(this);
        EntityDamageEvent.getHandlerList().unregister(this);
        PlayerQuitEvent.getHandlerList().unregister(this);

        if(dice != null)
            dice.remove();

        p.getPlayer().removePotionEffect(PotionEffectType.JUMP);

        MarioPartyAPI.manager.musicManager.unregisterPlayer(this);
        if(MarioPartyAPI.manager.globalBar != null)
            MarioPartyAPI.manager.globalBar.removePlayer(p);

        instances--;
    }


    public Promise openPowerUp(Anchor anchor){
        openingPowerup = true;
        ArmorStand armorStand = anchor.getPowerupEntity();

        return new Promise(){
            @Override
            public void run() {
                if(armorStand == null){
                    openingPowerup = false;
                    finish();
                    return;
                }

                new SequencedRunnable(pl){
                    @Override
                    public void run() {

                        int counter = 0;

                        // 4 Seconds
                        while(counter++ <= 16){
                            int randomInt = Utils.getRandomNumber(0, Utils.powerUpItems.size());
                            synced(() -> {
                                anchor.getPowerupEntity().setHelmet(Utils.powerUpItems.get(randomInt));
                                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BASS, 1, 1);
                            });
                            wait(250);
                        }

                        wait(3000);

                        if(anchor.getPowerupEntity().getHelmet().getItemMeta().getDisplayName().equalsIgnoreCase("Dice")){
                            synced(() -> {
                                giveDice();
                                anchor.getPowerupEntity().remove();
                            });
                        }else if(anchor.getPowerupEntity().getHelmet().getItemMeta().getDisplayName().equalsIgnoreCase("Coin")){
                            synced(() -> {
                                p.getInventory().addItem(new ItemStack(Material.GOLD_INGOT, 10));
                                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                                anchor.getPowerupEntity().remove();
                            });
                        }else if(anchor.getPowerupEntity().getHelmet().getItemMeta().getDisplayName().equalsIgnoreCase("Snapback")){
                            synced(() -> {
                                GamePlayer rp = MarioPartyAPI.manager.getPlayers().get(Utils.getRandomNumber(0, MarioPartyAPI.manager.getPlayers().size()));

                                if(rp.getPlayer().getDisplayName().equalsIgnoreCase(p.getDisplayName())){
                                    Bukkit.getServer().broadcastMessage("§e§l" + p.getName() + " §r§lwollte sich gerade selbst 3 Münzen stehlen! Komischer Kauz...");
                                }else{
                                    Bukkit.getServer().broadcastMessage("§e§l" + p.getName() + " §r§lhat §e§l" + rp.getPlayer().getName() + " §r§l3 Münzen gestohlen!");

                                    rp.removeCoin(3);
                                    giveCoin(3);
                                }


                                anchor.getPowerupEntity().remove();
                            });
                        }


                        openingPowerup = false;

                        finish();

                        super.run();
                    }
                }.runTask();

            }
        };

    }


    public void stopDice(){
        diceTask.cancel();
        diceRolling = false;
        dice.setHelmet(null);
        dice.remove();
        dice.getWorld().spawnParticle(Particle.SMOKE_NORMAL, dice.getLocation(), 10);
        timeoutTask.cancel();
        ChatUtils.sendToActionBar(p, "");
        onDiceHit(diceNumber);
    }


    public void onDiceHit(int diceNumber){

        PlayerHitDice event = new PlayerHitDice(this, diceNumber);

        Bukkit.getPluginManager().callEvent(event);
    }

    public Promise executeCustomField(FieldLocation loc) {
        inAction = true;
        inModule = true;
        if(loc.getFieldType() == FieldType.CUSTOM){
            MapModule module = MapModuleManager.getInstance().getById(loc.getFieldType().getId());
            if(module == null){
                return null;
            }
            return module.run(this);
        }
        return null;
    }


    public Promise executeBowserField(){
        inAction = true;
        return new Promise(){
            @Override
            public void run() {

                new SequencedRunnable(pl){
                    @Override
                    public void run() {

                        synced(() -> {
                            Location frontLocation = p.getEyeLocation().add(p.getLocation().getDirection().multiply(2)).add(0, -2, 0);
                            frontLocation.setYaw(frontLocation.getYaw() - 180);
                            dice = (ArmorStand) frontLocation.getWorld().spawnEntity(frontLocation, EntityType.ARMOR_STAND);
                            dice.setVisible(false);
                            dice.setSilent(true);
                            dice.setGravity(false);
                        });

                        int counter = 0;

                        // 4 Seconds
                        while(counter <= 16){

                            int randomInt = Utils.getRandomNumber(0, Utils.bowserItems.size());
                            ItemStack item = Utils.bowserItems.get(randomInt);
                            synced(() -> {
                                dice.setHelmet(item);
                                dice.setCustomName(item.getItemMeta().getDisplayName());
                                dice.setCustomNameVisible(true);
                            });

                            counter++;

                            wait(250);
                        }

                        if(dice.getHelmet().getItemMeta().getLore().get(0).equalsIgnoreCase("red")){
                            synced(() -> {

                                p.sendMessage("§4Oh nein! Die Hälfte deiner Münzen wird dir weggenommen!");
                                p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 1, 1);
                                removeAmount(Material.GOLD_INGOT, getCompleteAmount(Material.GOLD_INGOT) / 2);
                            });

                            wait(3000);

                            synced(() -> dice.remove());

                            inAction = false;
                            finish();
                        }else if(dice.getHelmet().getItemMeta().getLore().get(0).equalsIgnoreCase("yellow")){
                            synced(() -> {
                                p.sendMessage("§ePhu, es ist nichts passiert!");
                                p.playSound(p.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
                            });

                            wait(3000);

                            synced(() -> dice.remove());

                            inAction = false;
                            finish();
                        }else if(dice.getHelmet().getItemMeta().getLore().get(0).equalsIgnoreCase("green")){
//                                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
//                                p.getInventory().addItem(new ItemStack(Material.GOLD_INGOT, 53));

                            synced(() -> {
                                p.sendMessage("§eDu hast Glück! Du bekommst 3 Münzen!");
                                giveCoin(3);
                            });

                            wait(3000);

                            synced(() -> {
                                dice.remove();
                            });
                            inAction = false;
                            finish();
                        }
                        super.run();
                    }
                }.runTask();
            }
        };
    }



    public Promise giveCoin(){
        return giveCoin(1);
    }
    public Promise giveCoin(int amount){
        return new Promise(){
            @Override
            public void run() {
                p.getPlayer().getInventory().addItem(new ItemStack(Material.GOLD_INGOT, amount));

                MarioPartyAPI.manager.summonCoin(GamePlayer.this, amount);

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }.runTaskLaterAsynchronously(pl, 50);
            }
        };
    }



    public Promise removeCoin(){
        return removeCoin(1);
    }
    public Promise removeCoin(int amount){
        return new Promise(){
            @Override
            public void run() {
                removeAmount(Material.GOLD_INGOT, 1);
                p.getWorld().playSound(p.getLocation(), Sound.ENTITY_VILLAGER_HURT, 1, 1);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }.runTaskLaterAsynchronously(pl, 30);
            }
        };
    }



    public void giveDice(){
        if(diceRolling){
            return;
        }

        diceRolling = true;
        Location frontLocation = p.getEyeLocation().add(p.getLocation().getDirection().multiply(2)).add(0, -1.6, 0);
        frontLocation.setYaw(frontLocation.getYaw() - 180);
        dice = (ArmorStand) frontLocation.getWorld().spawnEntity(frontLocation, EntityType.ARMOR_STAND);
        dice.setVisible(false);
        dice.setSilent(true);
        dice.setGravity(false);
        dice.setCustomNameVisible(true);
        dice.setCustomName("§4§l6");
        frontLocation.getWorld().spawnParticle(Particle.CLOUD, frontLocation, 10);
        AtomicInteger lastNumber = new AtomicInteger(1);

        AtomicInteger timeout = new AtomicInteger(6);
        timeoutTask = new BukkitRunnable() {
            @Override
            public void run() {
                if(timeout.decrementAndGet() <= 0){
                    stopDice();
                    cancel();
                }
                ChatUtils.sendToActionBar(p, "§b§l" + timeout.get());
            }
        };
        timeoutTask.runTaskTimer(pl, 0, 20);

        diceTask = new BukkitRunnable() {
            @Override
            public void run() {
                diceNumber = Utils.getRandomNumber(1, 7);
                while(diceNumber == lastNumber.get()){
                    diceNumber = Utils.getRandomNumber(1, 7);
                }
                dice.setHelmet(new CustomHead(diceNumber + "", Utils.getDiceResource(diceNumber), Category.ALPHABET).getSkull());
                dice.setCustomName("§e§l" + diceNumber);
                p.playSound(p.getLocation(), Sound.BLOCK_LEVER_CLICK, 0.1f, 0.1f);
                dice.getWorld().spawnParticle(Particle.FIREWORKS_SPARK, dice.getLocation(), 5, 0.5, 2.5, 0.5, 0);
                lastNumber.set(diceNumber);
            }
        };
        diceTask.runTaskTimer(pl, 0, 3);

    }

    public boolean removeAmount(Material material, int amount){
        for(int i  = 0; i < p.getInventory().getContents().length; i++){
            ItemStack item = p.getInventory().getContents()[i];
            if(item == null)
                continue;

            if(item.getType() == material){

                System.out.println(item.getAmount() + ", " + amount);

                if(item.getAmount() < amount){
                    amount = Math.abs(item.getAmount() - amount);
                    p.getInventory().remove(new ItemStack(material, item.getAmount()));
                    return removeAmount(material, amount);
                }else if(item.getAmount() == amount){
                    p.getInventory().remove(new ItemStack(material, amount));
                    return true;
                }else{
                    item.setAmount(item.getAmount() - amount);
                    return true;
                }

            }
        }
        return false;
    }

    public boolean removeAmount(ItemStack itemStack, int amount){
        for(int i  = 0; i < p.getInventory().getContents().length; i++){
            ItemStack item = p.getInventory().getContents()[i];
            if(item == null)
                continue;

            if(ItemUtils.isSimilar(itemStack, item)){

                System.out.println(item.getAmount() + ", " + amount);

                if(item.getAmount() < amount){
                    amount = Math.abs(item.getAmount() - amount);
                    ItemStack rem = itemStack.clone();
                    rem.setAmount(amount);
                    p.getInventory().remove(rem);
                    return removeAmount(itemStack, amount);
                }else if(item.getAmount() == amount){
                    ItemStack rem = itemStack.clone();
                    rem.setAmount(amount);
                    p.getInventory().remove(rem);
                    return true;
                }else{
                    item.setAmount(item.getAmount() - amount);
                    return true;
                }

            }
        }
        return false;
    }


    public int getCompleteAmount(Material material){
        int amount = 0;
        for(ItemStack i : p.getInventory().getContents()){
            if(i == null)
                continue;
            if(i.getType() == material)
                amount = amount + i.getAmount();
        }
        return amount;
    }



    public void setMoveEnabled(boolean moveEnabled){
        if(moveEnabled){
            p.setWalkSpeed(0.2f);
            p.removePotionEffect(PotionEffectType.JUMP);
        }else{
            p.setWalkSpeed(0f);
            p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 99999999, 200, false, false));
        }
        this.moveEnabled = moveEnabled;
    }


    public PlayerScoreboardManager getScoreboardManager() {
        return scoreboardManager;
    }

    public Player getPlayer() {
        return p;
    }

    public boolean diceIsRolling() {
        return diceRolling;
    }

    public boolean isOpeningPowerup() {
        return openingPowerup;
    }

    @Deprecated
    public int getField() {
        return controller.getCurField();
    }

    public boolean isMoveEnabled() {
        return moveEnabled;
    }

    @Override
    public String toString() {
        return "GamePlayer{" +
                "p=" + p +
                ", pl=" + pl +
                ", diceRolling=" + diceRolling +
                ", dice=" + dice +
                ", diceTask=" + diceTask +
                ", diceNumber=" + diceNumber +
                ", timeoutTask=" + timeoutTask +
                ", powerUpTask=" + powerUpTask +
                ", openingPowerup=" + openingPowerup +
                '}';
    }

}
