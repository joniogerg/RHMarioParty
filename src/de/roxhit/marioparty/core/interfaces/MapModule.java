package de.roxhit.marioparty.core.interfaces;

import de.roxhit.marioparty.core.GamePlayer;
import de.roxhit.marioparty.core.utils.Promise;
import org.bukkit.plugin.Plugin;

public interface MapModule {

    String getIdentifier();
    Plugin getOwner();
    Promise run(GamePlayer p);
}
