package de.roxhit.marioparty.core.events;

import de.roxhit.marioparty.core.GamePlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerHitDice extends Event{
    private static final HandlerList handlers = new HandlerList();
    GamePlayer player;
    private int diceNumber;

    public PlayerHitDice(GamePlayer player, int diceNumber) {
        super();
        this.player = player;
        this.diceNumber = diceNumber;
    }

    public int getDiceNumber() {
        return diceNumber;
    }

    public GamePlayer getPlayer() {
        return player;
    }


    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
