package de.roxhit.marioparty.core;

import de.roxhit.marioparty.core.interfaces.Minigame;
import de.roxhit.marioparty.core.interfaces.MinigamePlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by jonio on 02.07.2017.
 */
public class Inventories {


    private static HashMap<String, ItemStack[]> mainInventories = new HashMap<>();


    public static ItemStack[] getMainInventory(String player){
        if(mainInventories.containsKey(player)){
            return mainInventories.get(player);
        }
        return null;
    }

    public static void setMainInventory(Player p){
        p.getInventory().clear();
        p.getInventory().setContents(getMainInventory(p.getName()));
    }

    public static void saveMainInventory(Player p){
        mainInventories.put(p.getName(), p.getInventory().getContents());
    }

    public static Inventory getGamesInventory(){
        Inventory inv = Bukkit.createInventory(null, 27, "§3§lGames");
        for(MinigamePlugin pl : MarioPartyAPI.manager.minigameManager.getMinigames()){
            Minigame minigame = pl.getMinigame();
            ItemStack item = minigame.getIcon();
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§l" + minigame.getName());
            meta.setLore(Utils.cutString(minigame.getDescription(), ChatColor.GRAY));
            item.setItemMeta(meta);

            inv.addItem(item);

        }
        return inv;
    }


    public static Inventory getScoreInventory(){

        Inventory inv = Bukkit.createInventory(null, 9*4, "§3Spielstand");

        MarioPartyAPI.manager.sortPlayerList();

        int lastPlace = 0;

        for(int i = 0; i < MarioPartyAPI.manager.getPlayers().size(); i++){
            GamePlayer p = MarioPartyAPI.manager.getPlayers().get(i);

            ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte)3);
            SkullMeta meta = (SkullMeta) skull.getItemMeta();
            meta.setOwner(p.getPlayer().getName());

            if(i != 0 && p.getCompleteAmount(Material.GOLD_INGOT) == MarioPartyAPI.manager.getPlayers().get(i - 1).getCompleteAmount(Material.GOLD_INGOT)){
                meta.setDisplayName("§l#" + lastPlace + " §6" + p.getPlayer().getName());
            }else{
                lastPlace++;
                meta.setDisplayName("§l#" + lastPlace + " §6" + p.getPlayer().getName());
            }

            meta.setLore(Arrays.asList("§eMünzen: " + p.getCompleteAmount(Material.GOLD_INGOT), "§Feld: " + p.getField()));
            skull.setItemMeta(meta);

            inv.addItem(skull);

        }


        return inv;
    }
}
