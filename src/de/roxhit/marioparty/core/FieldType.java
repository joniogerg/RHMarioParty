package de.roxhit.marioparty.core;

import org.bukkit.Material;

public enum FieldType{
    PLUSCOIN(Material.BLUE_GLAZED_TERRACOTTA, "PLUSCOIN"),
    MINUSCOIN(Material.RED_GLAZED_TERRACOTTA, "MINUSCOIN"),
    BOWSER(Material.REDSTONE_BLOCK, "BOWSER"),
    MINIGAME(Material.YELLOW_GLAZED_TERRACOTTA, "MINIGAME"),
    CUSTOM(Material.MAGENTA_GLAZED_TERRACOTTA, "CUSTOM"),
    NONE(Material.AIR, "NONE");

    public final Material material;
    public String id;

    FieldType(Material material, String id) {
        this.material = material;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}