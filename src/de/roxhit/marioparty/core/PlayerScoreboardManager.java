package de.roxhit.marioparty.core;

import org.bukkit.Bukkit;
import org.bukkit.scoreboard.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jonio on 01.08.2017.
 */
public class PlayerScoreboardManager {

    private GamePlayer p;
    private Scoreboard scoreboard;
    private HashMap<Integer, String> scoreboardEntries = new HashMap<>();

    public static final String SCOREBOARD_SIDEBAR_OBJECTIVE = "sidebar";



    public PlayerScoreboardManager(GamePlayer p) {
        this.p = p;
        initScoreboard();
    }

    public void initScoreboard(){
        ScoreboardManager manager = Bukkit.getScoreboardManager();

        this.scoreboard = manager.getNewScoreboard();

        Team team = scoreboard.registerNewTeam(p.getPlayer().getName());
        team.addEntry(p.getPlayer().getName());

        Objective objective = scoreboard.registerNewObjective(SCOREBOARD_SIDEBAR_OBJECTIVE, "");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        objective.setDisplayName(MarioPartyAPI.SCOREBOARD_HEADER);

        p.getPlayer().setScoreboard(scoreboard);

    }


    public void setScoreboard(HashMap<Integer, String> entries){
        resetScoreboard(entries);
        Objective objective = scoreboard.getObjective(SCOREBOARD_SIDEBAR_OBJECTIVE);


        for(int i = entries.size() - 1; i >= 0; i--) {
            Score score = objective.getScore("§" + i + "§r" + entries.get(i));
            score.setScore(i);

        }

        this.scoreboardEntries = entries;
    }

    public void updateScoreboard(){

        ArrayList<String> contents = new ArrayList<>();

        contents.add("");
        contents.add("§6§lDeine Platzierung: ");
        contents.add("§a#" + (MarioPartyAPI.manager.getPlayerPosition(p) == -1 ? "..." : MarioPartyAPI.manager.getPlayerPosition(p)));
        contents.add("");
        contents.add("§6§lDein Feld: ");
        contents.add("§a" + (p.controller.getCurField() + 1) + " von " + MarioPartyAPI.manager.map.getAllAnchors().size());
        contents.add("");


        updateScoreboard(contents);
    }

    public void updateScoreboard(List<String> contents){

        setScoreboard(convertList(invertList(contents)));
    }

    public void resetScoreboard(HashMap<Integer, String> entries){
        for(String entry : new ArrayList<>(scoreboard.getEntries())) {
            if(!entries.containsValue(entry)){
                scoreboard.resetScores(entry);
            }
        }

    }

    public HashMap<Integer, String> convertList(List<String> list){
        HashMap<Integer, String> hashMap = new HashMap<>();
        int i = 0;
        for(String s : list) {
            hashMap.put(i, s);
            i++;
        }
        return hashMap;
    }

    public ArrayList<String> invertList(List<String> list){
        ArrayList newList = new ArrayList();
        for(int i = list.size() - 1; i >= 0; i--){
            newList.add(list.get(i));
        }
        return newList;
    }
}
