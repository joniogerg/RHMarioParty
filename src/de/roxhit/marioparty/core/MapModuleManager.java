package de.roxhit.marioparty.core;

import de.roxhit.marioparty.core.interfaces.MapModule;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public class MapModuleManager implements Listener{

    private static Plugin pl;
    private static MapModuleManager instance;
    private static List<MapModule> modules = new ArrayList<>();

    private MapModuleManager(){

        loadModules();

        pl.getServer().getPluginManager().registerEvents(this, pl);

    }

    private void loadModules(){
        for(Plugin pl : Bukkit.getServer().getPluginManager().getPlugins()){
            if(pl instanceof MapModule){
                modules.add((MapModule)pl);
            }
        }
    }


    public MapModule getById(String id){
        for(MapModule module : modules){
            if(module.getIdentifier().equalsIgnoreCase(id)){
                return module;
            }
        }
        return null;
    }

    public static MapModuleManager getInstance(){
        if(instance == null){
            instance = new MapModuleManager();
        }
        return instance;
    }

    public static void setOwner(Plugin pl){
        MapModuleManager.pl = pl;
    }
}
