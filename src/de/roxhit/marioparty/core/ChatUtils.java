package de.roxhit.marioparty.core;

import net.minecraft.server.v1_12_R1.ChatMessageType;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutChat;
import net.minecraft.server.v1_12_R1.PlayerConnection;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class ChatUtils {

	
	public static void sendToActionBar(Player p, String message){
		if(message == null) message = "";
		message = ChatColor.translateAlternateColorCodes('&', message);
	    message = message.replaceAll("%P%", p.getDisplayName());
	    
	    PlayerConnection con = ((CraftPlayer)p).getHandle().playerConnection;
	    
	    IChatBaseComponent chat = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");
	    PacketPlayOutChat packet = new PacketPlayOutChat(chat, ChatMessageType.GAME_INFO);
	    
	    con.sendPacket(packet);
	    
	}

	public static  String formatTicks(int ticks){
		StringBuilder builder = new StringBuilder();

		int minutes = ticks / 20 / 60;
		int seconds = ticks / 20 - minutes * 60;

		builder.append((minutes <= 9 ? "0" + minutes : minutes) + ":" + (seconds <= 9 ? "0" + seconds : seconds));


		return builder.toString();
	}
}
