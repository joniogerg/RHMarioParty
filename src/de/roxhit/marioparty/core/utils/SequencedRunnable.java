package de.roxhit.marioparty.core.utils;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class SequencedRunnable implements Runnable{

    private Plugin pl;
    private BukkitTask asyncBukkitTask;

    public SequencedRunnable(Plugin pl) {
        this.pl = pl;
    }

    public void runTask(){
        SequencedRunnable main = this;
        asyncBukkitTask = new BukkitRunnable() {
            @Override
            public void run() {
                main.run();
            }
        }.runTaskAsynchronously(pl);
    }

    public void synced(Runnable run){
        BukkitRunnable synced = new BukkitRunnable() {
            @Override
            public synchronized void run() {
                run.run();
                notify();
            }
        };
        synced.runTask(pl);
        synchronized (synced){
            try {
                synced.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public void wait(int duration){
        try {
            Thread.sleep(duration);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        this.asyncBukkitTask.cancel();
    }
}
