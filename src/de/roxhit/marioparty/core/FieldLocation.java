package de.roxhit.marioparty.core;

import java.util.ArrayList;

public class FieldLocation  {


    /**
     *  Caution: The last anchor is the final anchor!
     */
    private ArrayList<Anchor> anchors;
    private FieldType fieldType;
    private int fieldNumber;

    public FieldLocation(ArrayList<Anchor> anchors, int fieldNumber, FieldType fieldType){
        this.anchors = anchors;
        this.fieldType = fieldType;
        this.fieldNumber = fieldNumber;
    }

    public Anchor getFinalAnchor(){
        return this.anchors.get(this.anchors.size() - 1);
    }

    public ArrayList<Anchor> getAnchors() {
        return anchors;
    }

    public void setAnchors(ArrayList<Anchor> anchors) {
        this.anchors = anchors;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }

    public void setFieldNumber(int fieldNumber){
        this.fieldNumber = fieldNumber;
    }

    public int getFieldNumber() {
        return fieldNumber;
    }
}
