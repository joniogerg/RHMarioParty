package de.roxhit.marioparty.core.config;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public abstract class Configuration extends YamlConfiguration{


    private Plugin pl;

    private File mainConfigFile;
    private boolean loaded = false;

    public Configuration(Plugin pl, String directory, String fileName){
        this.pl = pl;
        this.mainConfigFile = new File(pl.getDataFolder() + "/" + directory + (directory.isEmpty() ? "" : "/") + fileName +".yml");
        loadConfig();
    }

    public Configuration(Plugin pl, String fileName){
        this.pl = pl;
        this.mainConfigFile = new File(pl.getDataFolder() + "/" + fileName +".yml");
        loadConfig();
    }



    public abstract void addDefaults();


    public void loadConfig(){
        boolean empty = false;

        if(!new File(mainConfigFile.getParent()).exists()){
            new File(mainConfigFile.getParent()).mkdirs();
        }
        if(!mainConfigFile.exists()){
            try {
                mainConfigFile.createNewFile();
                empty = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            load(mainConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }

        if(empty){
            addDefaults();
            options().copyDefaults(true);
            saveConfig();
        }
        loaded = true;
    }

    public void saveConfig(){
        if(mainConfigFile == null) return;
        try {
            save(mainConfigFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public boolean isLoaded() {
        return loaded;
    }
}
