package de.roxhit.marioparty.core;

import de.roxhit.marioparty.core.config.GameConfiguration;
import de.roxhit.marioparty.core.events.MinigameFinished;
import de.roxhit.marioparty.core.events.PlayerControllerMovementFinished;
import de.roxhit.marioparty.core.events.PlayerHitDice;
import de.roxhit.marioparty.core.interfaces.Minigame;
import de.roxhit.marioparty.core.title.Styles;
import de.roxhit.marioparty.core.title.TitleAPI;
import de.roxhit.marioparty.core.utils.SequencedRunnable;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import java.util.ArrayList;

import static de.roxhit.marioparty.core.MarioPartyAPI.PREFIX_SYSTEM;

public class GameManager implements Listener {


    private Plugin pl;
    public GameMap map;
    public MinigameManager minigameManager;
    public GameConfiguration config;

    private ArrayList<GamePlayer> players = new ArrayList<>();
    private GamePlayer onTurn;
    private int onTurnIndex = 0;

    public BossBar globalBar;
    
    private int roundCount = 1;
    public MusicManager musicManager;


    private boolean running = false;

    public GameManager(Plugin pl){
        this.pl = pl;
        this.config = new GameConfiguration(pl);
        this.map = new GameMap(pl, config.getString(GameConfiguration.WORLD_CONFIGURATION));
        this.musicManager = new MusicManager(pl);
        this.minigameManager = new MinigameManager(pl, this);


        pl.getServer().getPluginManager().registerEvents(this, pl);


    }


    public void start(){

        globalBar = Bukkit.createBossBar("§3§lRunde " + roundCount, BarColor.BLUE, BarStyle.SEGMENTED_6);

        new SequencedRunnable(pl){
            @Override
            public void run() {
                synced(() -> {
                    for(GamePlayer p : players){
                        globalBar.addPlayer(p.getPlayer());
                        p.getPlayer().getInventory().clear();
                        p.getPlayer().getInventory().addItem(Utils.score);
                        musicManager.registerPlayer(p);
                        musicManager.startMusic(p);
                    }

                    players.forEach(p -> {
                        TitleAPI.sendTitle(p.getPlayer(), "Willkommen in Mario Party!", new Styles().setColor(ChatColor.GOLD), 20*1, 20*5, 20*1);
                        TitleAPI.sendSubtitle(p.getPlayer(), "Vesuche so viele Münzen wie möglich zu sammeln!", new Styles().setColor(ChatColor.YELLOW), 20*1, 20*5, 20*1);
                        p.getPlayer().teleport(map.spawn);
                        p.setMoveEnabled(false);
                    });
                });

                wait(7000);

                synced(() -> {
                    players.forEach(p -> {
                        TitleAPI.sendTitle(p.getPlayer(), "Die Spieler würfeln abwechselnd!", new Styles().setColor(ChatColor.GOLD), 20*1, 20*5, 20*1);
                        TitleAPI.sendSubtitle(p.getPlayer(), "Zuerst beginnt " + players.get(0).getPlayer().getName() + "!", new Styles().setColor(ChatColor.YELLOW), 20*1, 20*5, 20*1);
                    });
                });

                wait(7000);

                nextPlayer();
                super.run();
            }
        }.runTask();

    }

    private void nextRound(){

        new BukkitRunnable() {
            @Override
            public void run() {
                roundCount++;

                // GAME FINISHED

                if(roundCount > config.getInt(GameConfiguration.ROUNDS)){
                    sortPlayerList();
                    Bukkit.broadcastMessage("§7§n____________________________________");
                    Bukkit.broadcastMessage("");
                    Bukkit.broadcastMessage("§6§l§nDas Spiel ist zu Ende!");
                    Bukkit.broadcastMessage("");
                    Bukkit.broadcastMessage("§6§l" + players.get(0).getPlayer().getName() + " hat das Spiel gewonnen!");
                    Bukkit.broadcastMessage("");
                    Bukkit.broadcastMessage("§7§n____________________________________");
                    Bukkit.broadcastMessage("");


                    running = true;

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            globalBar.removeAll();
                        }
                    }.runTaskLater(pl, 20*20);
                    return;
                }


                // GAME NOT FINISHED


                globalBar.setProgress((double)roundCount / config.getInt(GameConfiguration.ROUNDS));
                globalBar.setTitle("§3§lRunde " + roundCount);

                nextPlayer();

            }
        }.runTaskLater(pl, 20*1);
    }


    public void nextPlayer(){

        globalBar.setProgress((double)roundCount / config.getInt(GameConfiguration.ROUNDS));

        if(onTurnIndex >= players.size()){
            onTurnIndex = 0;
            nextRound();
            return;
        }


        onTurn = players.get(onTurnIndex);


        TitleAPI.sendTitle(onTurn.getPlayer(), onTurn.getPlayer().getDisplayName() + " ist am Zug!", new Styles().setColor(ChatColor.GOLD), 5, 40, 5);
        TitleAPI.sendSubtitle(onTurn.getPlayer(), "Zum würfeln linksklicken!", new Styles().setColor(ChatColor.YELLOW), 5, 40, 5);

        onTurnIndex++;

        new SequencedRunnable(pl){
            @Override
            public void run() {

                wait(2500);


                synced(() -> onTurn.giveDice());


                super.run();
            }
        }.runTask();
    }



    @EventHandler
    public void onControllerMovementFinished(PlayerControllerMovementFinished e){
        System.out.println(e.isAsynchronous());
        if(e.getPlayer() == onTurn && !e.getPlayer().inAction){

            FieldLocation loc = map.fields.get(e.getFieldNumber());

            if(loc.getFinalAnchor().getPowerupEntity() != null){
                e.getPlayer().openPowerUp(loc.getFinalAnchor()).then(() -> executeFieldAction(e.getPlayer(), loc));
            }else{
                executeFieldAction(e.getPlayer(), loc);
            }


        }
    }

    public void executeFieldAction(GamePlayer p, FieldLocation loc){

        if(loc.getFieldType() == FieldType.PLUSCOIN){
            p.giveCoin().then(this::nextPlayer);
        }else if(loc.getFieldType() == FieldType.MINUSCOIN){
            p.removeCoin().then(this::nextPlayer);
        }else if(loc.getFieldType() == FieldType.BOWSER){
            p.executeBowserField().then(this::nextPlayer);
        }else if(loc.getFieldType() == FieldType.CUSTOM){
            p.executeCustomField(loc).then(() -> {
                p.inAction = false;
                p.inModule = false;
                nextPlayer();
            });
        }else if(loc.getFieldType() == FieldType.MINIGAME){
            Minigame minigame = minigameManager.getRandomMinigame();

            minigameManager.startMinigame(minigame);
        }

    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e){
        Player p = e.getPlayer();
        if(e.getItem() == null || e.getItem().getItemMeta().getDisplayName() == null)
            return;

        if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(Utils.score.getItemMeta().getDisplayName())){
            if(!getPlayer(p.getName()).inAction)
                p.openInventory(Inventories.getScoreInventory());
        }
    }


    @EventHandler
    public void onMinigameFinished(MinigameFinished e){
        nextPlayer();
    }


    @EventHandler
    public void onDiceHit(PlayerHitDice e){
        GamePlayer p = e.getPlayer();
        ChatUtils.sendToActionBar(p.getPlayer(), "§lDu hast eine " + e.getDiceNumber() + " gewürfelt!");

        TitleAPI.sendTitle(p.getPlayer(), "" + e.getDiceNumber(), new Styles().setColor(ChatColor.GOLD).setBold(true));

        e.getPlayer().controller.moveAlong(map.getNextAnchors(p.controller.getCurField(), e.getDiceNumber()));
    }


    public void summonCoin(GamePlayer p, int amount){


        Location loc = p.getPlayer().getEyeLocation().add(p.getPlayer().getLocation().getDirection().multiply(2)).add(0, -2.0, 0);

        ArmorStand text = (ArmorStand) loc.getWorld().spawnEntity(loc.clone().add(0, 0.3 ,0), EntityType.ARMOR_STAND);
        text.setVisible(false);
        text.setSilent(true);
        text.setGravity(false);
        text.setCustomNameVisible(true);
        text.setCustomName(amount <= 1 ? ("§e§lMünze (+1)") : ("§e§l" + amount + "Münzen (+" + amount + ")"));
        new BukkitRunnable() {
            @Override
            public void run() {
                text.remove();
            }
        }.runTaskLater(pl, 20*2);

        for(int i = 0; i < (amount >= 2 ? amount / 2 : amount); i++){
            Vector variation = new Vector(Utils.getRandomDouble(0D, 0.5D), 0, Utils.getRandomDouble(0D, 0.5D));
            ArmorStand armorStand = (ArmorStand) loc.getWorld().spawnEntity(loc.clone().add(loc.getDirection().multiply(0.3).add(amount >= 2 ? variation : new Vector())), EntityType.ARMOR_STAND);
            armorStand.setVisible(false);
            armorStand.setSilent(true);
            armorStand.setGravity(false);
            armorStand.setArms(true);
            armorStand.setHelmet(new ItemStack(Material.GOLD_INGOT));
            armorStand.setRightArmPose(new EulerAngle(80, -30,0));
            loc.getWorld().playSound(loc, Sound.BLOCK_NOTE_PLING, 3, 3);
            new BukkitRunnable() {
                @Override
                public void run() {
                    armorStand.remove();
                }
            }.runTaskLater(pl, 20*2);
        }


        loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc.clone().add(0,1.7, 0), 10);
    }


    public void setupGame(){


        // Setup Map

        this.map.setupWorld();


    }


    public void join(Player rawPlayer){

        if(isRegistered(rawPlayer.getDisplayName())){
            rawPlayer.sendMessage(PREFIX_SYSTEM + "Du bist bereits registriert!");
            return;
        }

        GamePlayer p = new GamePlayer(rawPlayer, pl);

        musicManager.registerPlayer(p);
        this.players.add(p);

        p.getPlayer().sendMessage(PREFIX_SYSTEM + "Du bist dem Spiel beigetreten!");
    }
    public void quit(Player rawPlayer){
        if(!isRegistered(rawPlayer.getDisplayName())){
            rawPlayer.sendMessage(PREFIX_SYSTEM + "Du bist noch nicht registriert!");
            return;
        }

        GamePlayer p = getPlayer(rawPlayer.getDisplayName());

        p.unregister();
        musicManager.unregisterPlayer(p);
        this.players.remove(p);

        p.getPlayer().sendMessage(PREFIX_SYSTEM + "Du hast das Spiel verlassen!");
    }

    public ArrayList<GamePlayer> getPlayers() {
        return players;
    }

    public GamePlayer getPlayer(String name){
        for(GamePlayer p : players){
            if(p.getPlayer().getDisplayName().equalsIgnoreCase(name)){
                return p;
            }
        }
        return null;
    }


    public int getPlayerPosition(GamePlayer player){
        sortPlayerList();

        int lastPlace = 0;

        for(int i = 0; i < getPlayers().size(); i++){
            GamePlayer p = getPlayers().get(i);
            if(p.getPlayer().getUniqueId().equals(player.getPlayer().getUniqueId())) {
                if (i != 0 && p.getCompleteAmount(Material.GOLD_INGOT) == getPlayers().get(i - 1).getCompleteAmount(Material.GOLD_INGOT)) {
                    return lastPlace;
                } else {
                    lastPlace++;
                    return lastPlace;
                }
            }
        }
        return -1;
    }


    public void sortPlayerList(){
        players.sort((o1, o2) -> o2.getCompleteAmount(Material.GOLD_INGOT) - o1.getCompleteAmount(Material.GOLD_INGOT));
    }


    public boolean isRegistered(String name){
        return getPlayer(name) != null;
    }

    public boolean isRunning() {
        return running;
    }

    public void disable() {

        for(Anchor loc : map.getAllAnchors().values()){
            if(loc.getPowerupEntity() != null){
                loc.getPowerupEntity().remove();
            }
        }
        for(GamePlayer p : getPlayers()){
            p.unregister();
        }

        if(globalBar != null)
            globalBar.removeAll();
    }
}
