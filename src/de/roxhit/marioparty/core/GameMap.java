package de.roxhit.marioparty.core;

import de.roxhit.heads.main.CustomHead;
import de.roxhit.marioparty.core.config.WorldConfiguration;
import de.walamana.fb64s.main.Category;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

import static de.roxhit.marioparty.core.config.WorldConfiguration.POWERUPS;
import static de.roxhit.marioparty.core.config.WorldConfiguration.SPAWN;
import static de.roxhit.marioparty.core.config.WorldConfiguration.WORLDNAME;

public class GameMap {

    public static final String CONFIG_ANCHOR_SECTION_PATH = "Anchors";
    public static final String CONFIG_FIELD_SECTION_PATH = "Fields";

    public Plugin pl;

    public World world;
    public String name;
    public HashMap<Integer, Anchor> anchors = new HashMap<>();
    public ArrayList<FieldLocation> fields = new ArrayList<>();
    public Location spawn;

    public boolean loaded = false;

    public WorldConfiguration config;

    public GameMap(Plugin pl, String name) {
        this.pl = pl;
        this.name = name;

        this.config = WorldConfiguration.getConfig(pl, name);
        this.config.loadConfig();

        if(!Utils.isLoaded(config.getString(WORLDNAME))){
            Bukkit.createWorld(new WorldCreator(config.getString(WORLDNAME)));
        }

        this.world = Bukkit.getWorld(config.getString(WORLDNAME));

        this.spawn = new Location(world,
                config.getDouble(SPAWN + "x"),
                config.getDouble(SPAWN + "y"),
                config.getDouble(SPAWN + "z"),
                config.getInt(SPAWN + "pitch"),
                config.getInt(SPAWN + "yaw"));

        new BukkitRunnable() {
            @Override
            public void run() {
                loadFields();
            }
        }.runTaskAsynchronously(pl);

    }




    public void setupWorld(){

        if(!loaded)
            return;

        for(int i = 0; i < config.getInt(POWERUPS); i++){
            spawnPowerup();
        }




    }




    /**
     *
     * Spawns a powerup randomly
     * Fields with powerups are bypassed
     *
     */

    private void spawnPowerup(){
        Random random = new Random();
        int randomInt = random.nextInt(fields.size());
        FieldLocation loc = fields.get(randomInt);
        if(loc.getFinalAnchor().getPowerupEntity() != null || loc.getFieldType() == FieldType.BOWSER || loc.getFieldType() == FieldType.MINIGAME){
            spawnPowerup();
            return;
        }

        ArmorStand armorStand = (ArmorStand) loc.getFinalAnchor().getWorld().spawnEntity(loc.getFinalAnchor(), EntityType.ARMOR_STAND);
        armorStand.setVisible(false);
        armorStand.setSilent(true);
        armorStand.setGravity(false);
        armorStand.setCustomNameVisible(true);
        armorStand.setCustomName("§5§lPowerUp!!!");
        armorStand.setHelmet(new CustomHead("PowerUp", Utils.BASE_64_POWERUP, Category.ALPHABET).getSkull());

        new BukkitRunnable() {
            @Override
            public void run() {
                Location loc = armorStand.getLocation();
                loc.setYaw(loc.getYaw() + 1);
                if(loc.getYaw() >= 180){
                    loc.setYaw(-180);
                }
                armorStand.teleport(loc);
            }
        }.runTaskTimer(pl, 0, 1);

        loc.getFinalAnchor().setPowerupEntity(armorStand);
    }



    /**
     *
     * Adds an new anchor to the maps configuration
     * @param loc
     */

    public int addAnchor(Location loc){

        int id = this.anchors.size();

        loc.setX(loc.getBlockX() + 0.5);
        loc.setY(loc.getBlockY() + 0.5);
        loc.setZ(loc.getBlockZ() + 0.5);

        Anchor anchor = new Anchor(id, loc, 0.1, 0, false);

        this.anchors.put(id, anchor);

        saveAnchor(id, anchor);

        return id;
    }


    /**
     * Modifies an existing anchor
     * @param anchorID
     * @param multiply
     * @param y
     * @param isSecondary
     */

    public void modifyAnchor(int anchorID, double multiply, double y, boolean isSecondary){

        Anchor anchor = this.anchors.get(anchorID);

        anchor.multiply = multiply;
        anchor.addY = y;
        anchor.setIsSecondaryAnchor(isSecondary);

        this.anchors.put(anchorID, anchor);

        saveAnchor(anchorID, anchor);
    }


    /**
     *
     * Saves an anchor to the default configuration
     *
     * @param id
     * @param anchor
     */
    public void saveAnchor(int id, Anchor anchor){


        String path = CONFIG_ANCHOR_SECTION_PATH + "." + id + ".";

        config.set(path + ".world", anchor.getWorld().getName());
        config.set(path + ".x", anchor.getX());
        config.set(path + ".y", anchor.getY());
        config.set(path + ".z", anchor.getZ());
        config.set(path + ".multiply", anchor.multiply);
        config.set(path + ".addY", anchor.addY);
        config.set(path + ".secondary", anchor.isSecondaryAnchor());

        config.saveConfig();

    }


    /**
     *
     * Save field location
     */

    public void saveFieldLocation(FieldLocation loc){
        String path = CONFIG_FIELD_SECTION_PATH + "." + loc.getFieldNumber() + ".";

        ArrayList<String> anchorsS = new ArrayList<>();
        for(Anchor anchor : loc.getAnchors()){
            anchorsS.add(anchor.getID() + "");
        }

        config.set(path + "anchors", anchorsS.toArray(new String[]{}));
        config.set(path + "fieldType", loc.getFieldType().toString());
        if(loc.getFieldType() == FieldType.CUSTOM){
            config.set(path + "module", loc.getFieldType().id);
        }

        config.saveConfig();
    }



    /**
     * Loads anchors from default bukkit yaml configuration
     */
    public void loadFields(){

        if(config.contains(CONFIG_ANCHOR_SECTION_PATH)){

            ConfigurationSection section = config.getConfigurationSection(CONFIG_ANCHOR_SECTION_PATH);
            fields.clear();
            for(String key : section.getKeys(false)){
                String path = CONFIG_ANCHOR_SECTION_PATH + "." + key + ".";

                Anchor anchor = new Anchor(Integer.parseInt(key), Bukkit.getWorld(config.getString(path + "world"))
                        , config.getDouble(path + "x")
                        , config.getDouble(path + "y")
                        , config.getDouble(path + "z")
                        , config.getDouble(path + "multiply")
                        , config.getDouble(path + "addY")
                        , config.getBoolean(path + "secondary"));

                anchors.put(Integer.parseInt(key), anchor);

            }
        }

        if(config.contains(CONFIG_FIELD_SECTION_PATH)){

            ConfigurationSection section = config.getConfigurationSection(CONFIG_FIELD_SECTION_PATH);
            fields.clear();
            for(String key : section.getKeys(false)){
                String path = CONFIG_FIELD_SECTION_PATH + "." + key + ".";

                ArrayList<Anchor> fieldAnchors = new ArrayList<>();

                List<String> anchorIDs = config.getStringList(path + "anchors");
                for(String s : anchorIDs){
                    fieldAnchors.add(anchors.get(Integer.parseInt(s)));
                }

                FieldLocation loc = new FieldLocation(fieldAnchors, Integer.parseInt(key), FieldType.valueOf(config.getString(path + "fieldType")));

                if(config.contains(path + "module")){
                    FieldType type = loc.getFieldType();
                    type.setId(config.getString(path + "module"));
                    loc.setFieldType(type);
                }

                fields.add(loc);

            }
        }


        System.out.println("Anchors [" + getAllAnchors().size() + "] and fields [" + fields.size() + "] for map \"" + name + "\" loaded successfully!");
        loaded = true;
    }


    /**
     * @param from      Field the player is cuurently standing at
     * @param amount    Amount of next anchors
     * @return          A list of anchors for movement
     */

    public ArrayList<Anchor> getNextAnchors(int from, int amount){
        ArrayList<Anchor> anchors = new ArrayList<>();
        int counter = 1;
        for(int i = 0; i < amount; i++){
            if(from + counter >= fields.size()){
                from = 0;
                counter = 1;
            }
            anchors.addAll(fields.get(from + counter).getAnchors());
            counter++;
        }
        return anchors;
    }


    /**
     * @param from  Field the player is currently standing at
     * @param to    Field the player should go to
     * @return      A list of anchors for movement
     */

    public ArrayList<Anchor> getAnchors(int from, int to){

        ArrayList<Anchor> anchors = new ArrayList<>();

        for(FieldLocation field : this.fields){

            if(field.getFieldNumber() > from ){

                anchors.addAll(field.getAnchors());
                if(field.getFieldNumber() == to){
                    break;
                }
            }
        }

        return anchors;
    }


    /**
     * @param fieldNumber The field you want to get to
     * @return      Returns the anchor of the field
     */

    public FieldLocation getField(int fieldNumber){
        for(FieldLocation field : fields){
            if(field.getFieldNumber() == fieldNumber){
                return field;
            }
        }
        return null;
    }


    public HashMap<Integer, Anchor> getAllAnchors(){
        return anchors;
    }

}
