package de.roxhit.marioparty.core;

import com.xxmicloxx.NoteBlockAPI.NBSDecoder;
import com.xxmicloxx.NoteBlockAPI.RadioSongPlayer;
import com.xxmicloxx.NoteBlockAPI.Song;
import com.xxmicloxx.NoteBlockAPI.SongPlayer;
import net.minecraft.server.v1_12_R1.GameProfileBanList;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jonio on 05.08.2017.
 */
public class MusicManager {

    private HashMap<GamePlayer, SongPlayer> registeredPlayers = new HashMap<>();
    private Status status = Status.STOPPED;
    private Plugin pl;
    public static final String BACKGROUND_FILE_PATH = "/background.nbs";


    public MusicManager(List<GamePlayer> registeredPlayers, Plugin pl) {
        this.pl = pl;

        init(registeredPlayers);
    }

    public MusicManager(Plugin pl) {
        this.pl = pl;
    }

    private void init(List<GamePlayer> players){
        players.forEach(player -> registerPlayer(player));

    }

    public void startMusic(GamePlayer p){
        registeredPlayers.get(p).setPlaying(true);
        registeredPlayers.get(p).setVolume(Byte.parseByte("10"));
        status = Status.RUNNING;
    }

    public void pauseMusic(GamePlayer p){
        registeredPlayers.get(p).setPlaying(false);
        status = Status.PAUSED;
    }

    public void stopMusic(GamePlayer p){
        registeredPlayers.get(p).destroy();
        status = Status.STOPPED;
    }

    public void registerPlayer(GamePlayer player){
        registeredPlayers.put(player, new RadioSongPlayer(NBSDecoder.parse(new File(pl.getDataFolder() + BACKGROUND_FILE_PATH))));
        registeredPlayers.get(player).addPlayer(player.getPlayer());
        registeredPlayers.get(player).setAutoDestroy(false);
        registeredPlayers.get(player).setFadeDuration(20);
        registeredPlayers.get(player).setFadeDone(20);
        new BukkitRunnable() {
            @Override
            public void run() {
                registeredPlayers.get(player).setVolume(Byte.parseByte("10"));
            }
        }.runTaskLater(pl, 20);

    }

    public SongPlayer getSongPlayer(GamePlayer p){
        return registeredPlayers.get(p);
    }

    public void unregisterPlayer(GamePlayer p){
        SongPlayer songPlayer = registeredPlayers.get(p);
        songPlayer.destroy();
        registeredPlayers.remove(p);
    }

    public boolean isRegistered(GamePlayer p){
        return registeredPlayers.containsKey(p);
    }

    public Status getStatus(){
        return status;
    }


    public enum Status{

        STOPPED,
        PAUSED,
        RUNNING;

    }

}
