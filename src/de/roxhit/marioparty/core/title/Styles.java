package de.roxhit.marioparty.core.title;

import org.bukkit.ChatColor;

/**
 * Created by jonio on 20.07.2017.
 */
public class Styles {

    private ChatColor color = ChatColor.WHITE;
    private boolean bold = false;
    private boolean italic = false;
    private boolean underlined = false;
    private boolean strikethrough = false;
    private boolean obfuscated = false;


    public ChatColor getColor() {
        return color;
    }

    public Styles setColor(ChatColor color) {
        this.color = color;
        return this;
    }

    public boolean isBold() {
        return bold;
    }

    public Styles setBold(boolean bold) {
        this.bold = bold;
        return this;
    }

    public boolean isItalic() {
        return italic;
    }

    public Styles setItalic(boolean italic) {
        this.italic = italic;
        return this;
    }

    public boolean isUnderlined() {
        return underlined;
    }

    public Styles setUnderlined(boolean underlined) {
        this.underlined = underlined;
        return this;
    }

    public boolean isStrikethrough() {
        return strikethrough;
    }

    public Styles setStrikethrough(boolean strikethrough) {
        this.strikethrough = strikethrough;
        return this;
    }

    public boolean isObfuscated() {
        return obfuscated;
    }

    public Styles setObfuscated(boolean obfuscated) {
        this.obfuscated = obfuscated;
        return this;
    }
}
