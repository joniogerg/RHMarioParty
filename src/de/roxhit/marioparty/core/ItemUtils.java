package de.roxhit.marioparty.core;

import org.bukkit.inventory.ItemStack;

/**
 * Created by jonio on 16.08.2017.
 */
public class ItemUtils {


    public static boolean isSimilar(ItemStack item1, ItemStack item2){
        return item1 != null &&
                item2 != null &&
                item1.getItemMeta().getDisplayName() != null &&
                item2.getItemMeta().getDisplayName() != null &&
                item1.getItemMeta().getDisplayName().equalsIgnoreCase(item2.getItemMeta().getDisplayName()) &&
                item1.getType() == item2.getType();
    }
}
