package de.roxhit.marioparty.core.setup;

import com.mysql.jdbc.Field;
import de.roxhit.Vector.Vectors;
import de.roxhit.marioparty.core.Anchor;
import de.roxhit.marioparty.core.FieldLocation;
import de.roxhit.marioparty.core.FieldType;
import de.roxhit.marioparty.core.PlayerController;
import de.roxhit.marioparty.core.utils.EntityController;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;

public class MapSetup implements Listener {


    private MapSetupState state = MapSetupState.MAIN_MENU;
    private Player p;
    private Plugin pl;
    private MapSetupManager manager;

    // Show field
    private BukkitTask showField = null;
    private int showFieldTimeout = 10;

    // Adjust anchor
    private Anchor adjustAnchor = null;
    private int adjustAnchorID = -1;

    // Add field
    private ArrayList<Anchor> addFieldAnchors = new ArrayList<>();
    private FieldType addFieldType = FieldType.NONE;

    // Anchor Visualizer
    private ArrayList<ArmorStand> visualizedAnchors = new ArrayList<>();
    private boolean visualized = false;

    public MapSetup(Player p, Plugin pl, MapSetupManager mapSetupManager) {
        this.p = p;
        this.pl = pl;
        this.manager = mapSetupManager;

        setInventory(MapSetupState.MAIN_MENU);

        this.pl.getServer().getPluginManager().registerEvents(this, pl);
    }


    @EventHandler
    public void onChat(AsyncPlayerChatEvent e){
        if(e.getPlayer().getDisplayName().equalsIgnoreCase(p.getDisplayName())){

            e.setCancelled(true);

            if(e.getMessage().equalsIgnoreCase("abort") || e.getMessage().equalsIgnoreCase("stop")){
                state = MapSetupState.MAIN_MENU;
                setInventory(state);
                e.setCancelled(true);
                return;
            }

            if(state == MapSetupState.SHOW_FIELD_CHAT){

                try{
                    int index = Integer.parseInt(e.getMessage());

                    if(index >= 0 && index <= manager.toSetup.fields.size() - 1){

                        if(showField != null){
                            showField.cancel();
                        }

                        System.out.println(manager.toSetup.world.getName());

                        showField = new BukkitRunnable() {
                            @Override
                            public void run() {

                                if(showFieldTimeout == 0){
                                    showFieldTimeout = 10;
                                    cancel();
                                    showField = null;
                                    return;
                                }

                                for(Anchor anchor : manager.toSetup.getField(index).getAnchors()){
                                    manager.toSetup.world.spawnParticle(anchor.isSecondaryAnchor() ? Particle.HEART : Particle.NOTE, anchor, 5);
                                }

                                showFieldTimeout--;

                            }
                        }.runTaskTimer(pl, 0, 10);

                        state = MapSetupState.MAIN_MENU;

                    }else{
                        throw new IllegalArgumentException("Expected int between 0 and " + (manager.toSetup.fields.size() - 1));
                    }

                }catch (IllegalArgumentException ex){
                    e.getPlayer().sendMessage("§4You have to enter a valid number");
                }

                e.setCancelled(true);

            }


            if(state == MapSetupState.ANCHOR_ADJUST_SELECT){
                try{
                    int key = Integer.parseInt(e.getMessage());

                    if(manager.toSetup.getAllAnchors().containsKey(key)){
                        adjustAnchor = manager.toSetup.getAllAnchors().get(key);
                        adjustAnchorID = key;
                        state = MapSetupState.ANCHOR_ADJUST_MENU;
                        setInventory(state);
                    }else{
                        throw new IllegalArgumentException("Anchor not found!");
                    }
                }catch (IllegalArgumentException ex){
                    e.getPlayer().sendMessage("§4You have to enter a valid number");
                }
                e.setCancelled(true);
            }

            if(state == MapSetupState.ADD_FIELD_ANCHOR){
                try{
                    addFieldAnchors.clear();
                    for(String s : e.getMessage().split(" ")){
                        int index = Integer.parseInt(s);
                        if(!manager.toSetup.getAllAnchors().containsKey(index)){
                            throw new IllegalArgumentException("Anchor does not exist");
                        }else{
                            addFieldAnchors.add(manager.toSetup.getAllAnchors().get(index));
                        }
                    }

                    e.getPlayer().sendMessage("Enter the type for the field");
                    e.getPlayer().sendMessage("- PLUSCOIN");
                    e.getPlayer().sendMessage("- MINUSCOIN");
                    e.getPlayer().sendMessage("- BOWSER");
                    e.getPlayer().sendMessage("- CUSTOM");

                    state = MapSetupState.ADD_FIELD_TYPE;
                }catch (IllegalArgumentException ex){
                    e.getPlayer().sendMessage("§4You have to enter valid numbers");
                }
                e.setCancelled(true);
            }

            if(state == MapSetupState.ADD_FIELD_TYPE){
                try{

                    addFieldType = FieldType.valueOf(e.getMessage().toUpperCase());

                    if(addFieldType == FieldType.CUSTOM){
                        state = MapSetupState.ADD_FIELD_MODULE;

                        // TODO: Save field module

                    }else{
                        FieldLocation loc = new FieldLocation(addFieldAnchors, manager.toSetup.fields.size(), addFieldType);

                        manager.toSetup.saveFieldLocation(loc);
                        manager.toSetup.fields.add(loc);

                        e.getPlayer().sendMessage("§3Field successfully set up!");
                        state = MapSetupState.MAIN_MENU;
                    }

                }catch (IllegalArgumentException ex){
                    e.getPlayer().sendMessage("§4Enter a valid field type!");
                }
                e.setCancelled(true);
            }

            if(state == MapSetupState.ADD_FIELD_MODULE){
                addFieldType.setId(e.getMessage());
                FieldLocation loc = new FieldLocation(addFieldAnchors, manager.toSetup.fields.size(), addFieldType);

                manager.toSetup.saveFieldLocation(loc);
                manager.toSetup.fields.add(loc);

                e.getPlayer().sendMessage("§3Field successfully set up!");
                state = MapSetupState.MAIN_MENU;
                e.setCancelled(true);
            }

            if(state == MapSetupState.MOVE_P_FIELD_SELECT){
                try{
                    String[] args = e.getMessage().split(" ");

                    if(args.length <= 1){
                        throw new IllegalArgumentException("Not enough arguments");
                    }

//                    Anchor start = manager.toSetup.anchors.get(Integer.parseInt(args[0]));

                    ArrayList<Anchor> anchors = manager.toSetup.getAnchors(Integer.parseInt(args[0]), Integer.parseInt(args[1]));

                    state = MapSetupState.MOVE_P_FIELD_RUNNING;

                    new BukkitRunnable() {
                        @Override
                        public void run() {

//                            p.teleport(start);

                            EntityController entityController = new EntityController(pl, p, manager.toSetup);
                            entityController.moveAlong(anchors).then(() -> {
                                state = MapSetupState.MAIN_MENU;
                                setInventory(state);
                            });
                        }
                    }.runTaskLater(pl, 20);

                }catch (IllegalArgumentException ex){
                    e.getPlayer().sendMessage("§4Enter 2 valid numbers!");
                }
                e.setCancelled(true);
            }



            if(state == MapSetupState.ANCHOR_ADJUST_JUMP){
                try{
                    double value = Double.valueOf(e.getMessage());

                    adjustAnchor.addY = value;
                    manager.toSetup.modifyAnchor(adjustAnchorID, adjustAnchor.multiply, value, adjustAnchor.isSecondaryAnchor());

                    state = MapSetupState.ANCHOR_ADJUST_MENU;

                    setInventory(state);
                }catch (IllegalArgumentException ex){
                    e.getPlayer().sendMessage("§4Enter a valid double!");
                }
            }


            if(state == MapSetupState.ANCHOR_ADJUST_AMPLIFIER){
                try{
                    double value = Double.valueOf(e.getMessage());

                    adjustAnchor.multiply = value;
                    manager.toSetup.modifyAnchor(adjustAnchorID, value, adjustAnchor.addY, adjustAnchor.isSecondaryAnchor());

                    state = MapSetupState.ANCHOR_ADJUST_MENU;

                    setInventory(state);
                }catch (IllegalArgumentException ex){
                    e.getPlayer().sendMessage("§4Enter a valid double!");
                }
            }

        }
    }



    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        if(e.getPlayer().getDisplayName().equalsIgnoreCase(p.getDisplayName())){


            e.setCancelled(true);
            if(state == MapSetupState.MAIN_MENU){

                if(e.getItem() != null){
                    if(e.getItem().getType() == Material.BARRIER){
                        manager.setPlayerSetupMode(p, false);
                        p.getInventory().clear();
                        p.sendMessage("You left the setup mode");
                    }else if(e.getItem().getType() == Material.COMPASS){
                        state = MapSetupState.SHOW_FIELD_CHAT;
                        if(manager.toSetup.fields == null || manager.toSetup.getAllAnchors().size() == 0){
                            p.sendMessage("§cThere are no fiels set up yet" );
                            return;
                        }
                        p.sendMessage("Enter the number of the field [0 - " + (manager.toSetup.fields.size() - 1) +"] " );
                    }else if(e.getItem().getType() == Material.TRIPWIRE_HOOK){
                        int id = manager.toSetup.addAnchor(e.getPlayer().getLocation());
                        p.sendMessage("New anchor added with id " + id);
                    }else if(e.getItem().getType() == Material.WATCH){
                        state = MapSetupState.ANCHOR_ADJUST_SELECT;
                        p.sendMessage("Enter the number of the anchor [ 1 - " + manager.toSetup.getAllAnchors().size() + "]");
                    }else if(e.getItem().getType() == Material.EMERALD_BLOCK){
                        state = MapSetupState.ADD_FIELD_ANCHOR;
                        p.sendMessage("Enter the numbers of anchors you want to use for this field (separated by whitespace)");
                    }else if(e.getItem().getType() == Material.NAME_TAG){
                        if(visualized){
                            for(ArmorStand a : visualizedAnchors){
                                a.remove();
                            }
                            visualized = false;
                        }else{

                            for(Anchor a : manager.toSetup.getAllAnchors().values()){

                                ArmorStand armorStand = (ArmorStand) a.getWorld().spawnEntity(a, EntityType.ARMOR_STAND);
                                armorStand.setVisible(false);
                                armorStand.setSilent(true);
                                armorStand.setGravity(false);
                                armorStand.setCustomNameVisible(true);
                                armorStand.setCustomName("§4§l" + a.getID());

                                FieldLocation finalF = a.getFieldLocation(manager.toSetup);
                                if(finalF != null){
                                    ArmorStand armorStandField = (ArmorStand) a.getWorld().spawnEntity(a.clone().add(0, 0.5, 0), EntityType.ARMOR_STAND);
                                    armorStandField.setVisible(false);
                                    armorStandField.setSilent(true);
                                    armorStandField.setGravity(false);
                                    armorStandField.setCustomNameVisible(true);
                                    armorStandField.setCustomName("§3§l" + a.getFieldLocation(manager.toSetup).getFieldNumber());

                                    visualizedAnchors.add(armorStandField);
                                }

                                visualizedAnchors.add(armorStand);

                            }

                            visualized = true;

                        }
                    }else if(e.getItem().getType() == Material.DIAMOND_BOOTS){
                        state = MapSetupState.MOVE_P_MENU;
                        setInventory(state);
                    }
                }

                e.setCancelled(true);

            }else if(state == MapSetupState.ANCHOR_ADJUST_MENU){

                if(e.getItem() != null){

                    if(e.getItem().getType() == Material.BARRIER){
                        state = MapSetupState.MAIN_MENU;
                        setInventory(state);
                        return;
                    }else if(e.getItem().getType() == Material.FISHING_ROD){
                        state = MapSetupState.ANCHOR_ADJUST_JUMP;
                        p.sendMessage("Enter a value defining the intensity of the jump effect");
                        return;
                    }else if(e.getItem().getType() == Material.DIODE){
                        state = MapSetupState.ANCHOR_ADJUST_AMPLIFIER;
                        p.sendMessage("Enter a value defining the intensity of the amplifier");
                        return;
                    }else if(e.getItem().getType() == Material.TORCH){

                        adjustAnchor.setIsSecondaryAnchor(!adjustAnchor.isSecondaryAnchor());

                        manager.toSetup.modifyAnchor(adjustAnchorID, adjustAnchor.multiply, adjustAnchor.addY, adjustAnchor.isSecondaryAnchor());

                        setInventory(state);

                        return;
                    }




                }



            }else if(state == MapSetupState.MOVE_P_MENU){

                if(e.getItem() != null){

                    if(e.getItem().getType() == Material.BARRIER){
                        state = MapSetupState.MAIN_MENU;
                        setInventory(state);
                        return;
                    }

                    if(e.getItem().getType() == Material.LIGHT_BLUE_GLAZED_TERRACOTTA){
                        state = MapSetupState.MOVE_P_FIELD_SELECT;
                        p.sendMessage("Enter the start and end number of the anchors (separated by whitespace)");
                        return;
                    }


                }
            }



        }
    }



    public void setInventory(MapSetupState state){

        p.getInventory().clear();

        if(state == MapSetupState.MAIN_MENU){

            ItemStack exitSetupMode = new ItemStack(Material.BARRIER);
            ItemMeta exitSetupModeM = exitSetupMode.getItemMeta();
            exitSetupModeM.setDisplayName("§4Exit setup mode");
            exitSetupMode.setItemMeta(exitSetupModeM);

            ItemStack moveP = new ItemStack(Material.DIAMOND_BOOTS);
            ItemMeta movePM = moveP.getItemMeta();
            movePM.setDisplayName("§2Move along anchors / fields");
            moveP.setItemMeta(movePM);

            ItemStack showField = new ItemStack(Material.COMPASS);
            ItemMeta showFieldM = showField.getItemMeta();
            showFieldM.setDisplayName("§6Show fields anchors for 5 sec");
            showField.setItemMeta(showFieldM);

            ItemStack addField = new ItemStack(Material.EMERALD_BLOCK);
            ItemMeta addFieldM = addField.getItemMeta();
            addFieldM.setDisplayName("§aAdd new field");
            addField.setItemMeta(addFieldM);

            ItemStack addAnchor = new ItemStack(Material.TRIPWIRE_HOOK);
            ItemMeta addAnchorM = addAnchor.getItemMeta();
            addAnchorM.setDisplayName("§6Add new anchor");
            addAnchor.setItemMeta(addAnchorM);

            ItemStack adjustAnchor = new ItemStack(Material.WATCH);
            ItemMeta adjustAnchorM = adjustAnchor.getItemMeta();
            adjustAnchorM.setDisplayName("§6Adjust anchor");
            adjustAnchor.setItemMeta(adjustAnchorM);

            ItemStack vizualize = new ItemStack(Material.NAME_TAG);
            ItemMeta vizualizeM = vizualize.getItemMeta();
            vizualizeM.setDisplayName("§6Vizualize anchors");
            vizualize.setItemMeta(vizualizeM);

            p.getInventory().setItem(0, exitSetupMode);
            p.getInventory().setItem(2, moveP);
            p.getInventory().setItem(3, showField);
            p.getInventory().setItem(4, addField);
            p.getInventory().setItem(5, addAnchor);
            p.getInventory().setItem(6, adjustAnchor);
            p.getInventory().setItem(7, vizualize);

        }

        if(state == MapSetupState.ANCHOR_ADJUST_MENU){

            ItemStack exitSetupMode = new ItemStack(Material.BARRIER);
            ItemMeta exitSetupModeM = exitSetupMode.getItemMeta();
            exitSetupModeM.setDisplayName("§4Go back");
            exitSetupMode.setItemMeta(exitSetupModeM);


            ItemStack jumpEffect = new ItemStack(Material.FISHING_ROD);
            ItemMeta jumpEffectM = jumpEffect.getItemMeta();
            jumpEffectM.setDisplayName("§3Jump effect (" + adjustAnchor.addY + ")");
            jumpEffect.setItemMeta(jumpEffectM);


            ItemStack amplifierEffect = new ItemStack(Material.DIODE);
            ItemMeta amplifierEffectM = amplifierEffect.getItemMeta();
            amplifierEffectM.setDisplayName("§8Amplifier effect (" + adjustAnchor.multiply + ")");
            amplifierEffect.setItemMeta(amplifierEffectM);


            ItemStack isSecondary = new ItemStack(Material.TORCH);
            ItemMeta isSecondaryM = isSecondary.getItemMeta();
            isSecondaryM.setDisplayName("§3Is secondary (" + (adjustAnchor.isSecondaryAnchor() ? "ON" : "OFF") + ")");
            isSecondary.setItemMeta(isSecondaryM);


            p.getInventory().setItem(0, exitSetupMode);
            p.getInventory().setItem(3, jumpEffect);
            p.getInventory().setItem(4, amplifierEffect);
            p.getInventory().setItem(5, isSecondary);

        }

        if(state == MapSetupState.MOVE_P_MENU){
            ItemStack exitSetupMode = new ItemStack(Material.BARRIER);
            ItemMeta exitSetupModeM = exitSetupMode.getItemMeta();
            exitSetupModeM.setDisplayName("§4Go back");
            exitSetupMode.setItemMeta(exitSetupModeM);


            ItemStack moveOnField = new ItemStack(Material.LIGHT_BLUE_GLAZED_TERRACOTTA);
            ItemMeta moveOnFieldrM = moveOnField.getItemMeta();
            moveOnFieldrM.setDisplayName("§6Move along field");
            moveOnField.setItemMeta(moveOnFieldrM);

            p.getInventory().setItem(0, exitSetupMode);
            p.getInventory().setItem(3, moveOnField);
        }


    }


    /**
     * Automatically removes player from setup mode if he quits
     */

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        manager.setPlayerSetupMode(e.getPlayer(), false);
    }


    public void removeAllArmorStands(){
        for(ArmorStand a : visualizedAnchors){
            a.remove();
        }
    }

}
