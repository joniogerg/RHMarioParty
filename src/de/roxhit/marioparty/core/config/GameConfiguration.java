package de.roxhit.marioparty.core.config;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class GameConfiguration extends Configuration {


    public static final String WORLD_CONFIGURATION = "game.world-configuration";
    public static final String ROUNDS = "game.rounds";

    public GameConfiguration(Plugin pl) {
        super(pl, "gameConfig");
    }

    @Override
    public void addDefaults() {

        addDefault(WORLD_CONFIGURATION, "world");
        addDefault(ROUNDS, 20);
    }


}
