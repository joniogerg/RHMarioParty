package de.roxhit.marioparty.core;

import de.roxhit.marioparty.core.events.MinigameFinished;
import de.roxhit.marioparty.core.interfaces.Minigame;
import de.roxhit.marioparty.core.interfaces.MinigamePlugin;
import de.roxhit.marioparty.core.title.Styles;
import de.roxhit.marioparty.core.title.TitleAPI;
import de.roxhit.marioparty.core.utils.SequencedRunnable;
import org.bukkit.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import javax.rmi.CORBA.Util;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by jonio on 26.06.2017.
 */
public class MinigameManager {

    private Plugin pl;
    private GameManager manager;
    private ArrayList<MinigamePlugin> minigames = new ArrayList<>();
    private ArrayList<Minigame> playedMinigames = new ArrayList<>();

    public boolean minigameIsRunning = false;
    public Minigame currentMinigame = null;

    public MinigameManager(Plugin pl, GameManager manager){

        this.pl = pl;
        this.manager = manager;



        for(Plugin plugins : Bukkit.getServer().getPluginManager().getPlugins()){
            if(plugins instanceof MinigamePlugin){
                MinigamePlugin plugin = (MinigamePlugin) plugins;

                minigames.add(plugin);
            }
        }

        for(MinigamePlugin m : minigames){
            System.out.println("MinigameName: " + m.getMinigame().getName());
        }

    }


    public void startMinigame(Minigame minigame){
        if(minigameIsRunning){
            return;
        }

        currentMinigame = minigame;



        minigameIsRunning = true;

        manager.getPlayers().forEach(p -> {
            Inventories.saveMainInventory(p.getPlayer());
            p.getPlayer().getInventory().clear();
            p.getPlayer().setGameMode(GameMode.ADVENTURE);
        });


        manager.getPlayers().forEach(p -> {
            TitleAPI.sendTitle(p.getPlayer(), "Ein Minispiel wird gespielt!", new Styles().setColor(ChatColor.GOLD), 20*5);
            TitleAPI.sendSubtitle(p.getPlayer(), minigame.getName(), new Styles().setColor(ChatColor.YELLOW).setBold(true).setUnderlined(true), 20*5);
        });

        Bukkit.broadcastMessage("§7§n____________________________________");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§6§l§nEs wird gespielt:");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§e" + minigame.getName());
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage(minigame.getDescription());
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§7§n____________________________________");
        Bukkit.broadcastMessage("");
        new BukkitRunnable() {
            @Override
            public void run() {
                manager.getPlayers().forEach(p -> p.setMoveEnabled(true));
                teleportPlayersIntoMinigame(minigame);
                minigame.startGame();
            }
        }.runTaskLater(pl, 20*5);


    }

    public void stopMinigame(Minigame minigame){
        if(!minigameIsRunning){
            return;
        }

        for(GamePlayer p : minigame.getGamePlayer()){
            Inventories.setMainInventory(p.getPlayer());
            Sounds.playSound(p.getPlayer(), pl);
            p.getPlayer().setHealth(20);
            p.getPlayer().setFoodLevel(20);
            p.getPlayer().setGameMode(GameMode.ADVENTURE);
        }

        if(minigame.getGameTask() != null)
            minigame.getGameTask().cancel();

        Result result = minigame.getResults();

        GamePlayer first = result.getPlayer(1);
        GamePlayer second = null;
        GamePlayer last = null;
        if(result.getResults().containsKey(2)){
            second = result.getPlayer(2);
        }
        if(result.getResults().containsKey(3)){
            last = result.getPlayer(3);
        }

        Bukkit.broadcastMessage("§7§m-------------------------------------");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§6§l§nDas Spiel ist zu Ende!");
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§a1# §8» §r" + first.getPlayer().getName() + " §8[§e+3§8]");
        if(second != null)
            Bukkit.broadcastMessage("§a2# §8» §r" + second.getPlayer().getName() + " §8[§e+2§8]");
        if(last != null)
            Bukkit.broadcastMessage("§a3# §8» §r" + last.getPlayer().getName() + " §8[§e+1§8]");
        Bukkit.broadcastMessage("");
        for(GamePlayer p : minigame.getGamePlayer()){
            p.getPlayer().sendMessage("Dein Platz §8» §a" + result.getPlace(p) + "#");
        }
        Bukkit.broadcastMessage("");
        Bukkit.broadcastMessage("§7§m-------------------------------------");
        Bukkit.broadcastMessage("");

        GamePlayer finalSecond = second;
        GamePlayer finalLast = last;

        new SequencedRunnable(pl){
            @Override
            public void run() {

                wait(4000);

                synced(() -> teleportPlayersIntoMainMap());

                wait(1500);

                synced(() -> {
                    first.giveCoin(3);
                    if(finalSecond != null) {
                        finalSecond.giveCoin(2);
                    }
                    if(finalLast != null) {
                        finalLast.giveCoin();
                    }
                });

                synced(minigame::unregisterListener);

                wait(1500);

                minigameIsRunning = false;
                currentMinigame = null;

                synced(() -> Bukkit.getPluginManager().callEvent(new MinigameFinished(minigame)));

                super.run();
            }
        }.runTask();

        playedMinigames.add(minigame);


    }



    private void teleportPlayersIntoMinigame(Minigame minigame){
        int loc = 0;
        for(int i = 0; i < minigame.getGamePlayer().size(); i++){
            if(minigame.getSpawnLocations().size() == 1){
                minigame.getGamePlayer().get(i).getPlayer().teleport(minigame.getSpawnLocations().get(0));
            }else{
                if(minigame.getSpawnLocations().size() < minigame.getGamePlayer().size() && loc == minigame.getSpawnLocations().size() - 1){
                    loc = 0;
                }
                minigame.getGamePlayer().get(i).getPlayer().teleport(minigame.getSpawnLocations().get(loc));
            }
//            minigame.getGamePlayer().get(i).setInMainMap(false);
            minigame.getGamePlayer().get(i).getPlayer().setGameMode(GameMode.ADVENTURE);
            loc++;
        }
    }



    public Minigame getRandomMinigame(){
        Minigame m = minigames.get(Utils.getRandomNumber(0, minigames.size())).getMinigame();


        if(playedMinigames.size() >= minigames.size()){
            playedMinigames.clear();
            return m;
        }

        for(Minigame minigames : playedMinigames){
            if(minigames.getName().equalsIgnoreCase(m.getName())) {

                return getRandomMinigame();

            }
        }

        return m;
    }

    public Minigame getMinigameByName(String name){
        for(MinigamePlugin pl : minigames){
            if(pl.getMinigame().getName().equalsIgnoreCase(name))
                return pl.getMinigame();
        }
        return null;
    }

    public ArrayList<MinigamePlugin> getMinigames(){
        return minigames;
    }

    public void teleportPlayersIntoMainMap(){
        for(GamePlayer p : manager.getPlayers()){
            Location loc = manager.map.fields.get(p.controller.getCurField()).getFinalAnchor();
            if(loc == null){
                p.getPlayer().teleport(new Location(Bukkit.getWorld("world"), 940, 20, 1870));
                continue;
            }
            p.getPlayer().teleport(loc);
            p.getPlayer().getWorld().spawnParticle(Particle.DRIP_LAVA, loc, 10);
            p.getPlayer().setHealth(20);
            p.getPlayer().setSaturation(20);
//            p.setInMainMap(true);
        }
    }
}
