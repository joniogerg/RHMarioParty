package de.roxhit.marioparty.core.config;

import org.bukkit.plugin.Plugin;

import java.io.File;

public class WorldConfiguration extends Configuration {

    public static final String WORLDNAME = "worldName";
    public static final String SPAWN = "spawn.";
    public static final String POWERUPS = "powerups";

    private static WorldConfiguration instance;

    public WorldConfiguration(Plugin pl, String worldName) {
        super(pl, "worlds", worldName);
    }

    @Override
    public void addDefaults() {
        addDefault(POWERUPS, 0);
        addDefault(WORLDNAME, "world");
        addDefault(SPAWN + "x", 0);
        addDefault(SPAWN + "y", 100);
        addDefault(SPAWN + "z", 0);
        addDefault(SPAWN + "pitch", 0);
        addDefault(SPAWN + "yaw", 0);
    }

    public static boolean configExists(File dataFolder, String worldConfig){
        return new File(dataFolder + "/worlds/" + worldConfig + ".yml").exists();
    }

    public static WorldConfiguration getConfig(Plugin pl, String worldConfig){
        return new WorldConfiguration(pl, worldConfig);
    }
}
