package de.roxhit.marioparty.core.events;

import de.roxhit.marioparty.core.GamePlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerControllerMovementFinished extends Event{
    private static final HandlerList handlers = new HandlerList();
    GamePlayer player;
    int fieldNumber;

    public PlayerControllerMovementFinished(GamePlayer player, int fieldNumber) {
        super();
        this.player = player;
        this.fieldNumber = fieldNumber;
    }

    public int getFieldNumber() {
        return fieldNumber;
    }

    public GamePlayer getPlayer() {
        return player;
    }


    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
