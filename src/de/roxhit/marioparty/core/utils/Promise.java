package de.roxhit.marioparty.core.utils;

public class Promise implements Runnable{


    private Runnable run;
    private boolean hasBeenFinished = false;

    public Promise() {
        this.run();
    }

    public void then(Runnable run){

        this.run = run;
    }


    @Override
    public void run() {
    }

    public void finish(){
        if(hasBeenFinished){
            throw new IllegalStateException("Promise has already been finished!");
        }
        if(run != null){
            hasBeenFinished = true;
            run.run();
        }
    }
}
