package de.roxhit.marioparty.core;

import de.roxhit.heads.main.CustomHead;
import de.roxhit.marioparty.core.config.Configuration;
import de.roxhit.marioparty.core.config.GameConfiguration;
import de.roxhit.marioparty.core.config.WorldConfiguration;
import de.roxhit.marioparty.core.interfaces.Minigame;
import de.roxhit.marioparty.core.interfaces.MinigamePlugin;
import de.roxhit.marioparty.core.setup.MapSetupManager;
import de.roxhit.marioparty.core.title.Styles;
import de.roxhit.marioparty.core.title.TitleAPI;
import de.walamana.fb64s.main.Category;
import org.bukkit.*;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.EulerAngle;
import sun.security.krb5.Config;

import java.util.*;

public class MarioPartyAPI extends JavaPlugin implements Listener {


    @Deprecated
    /**
     *
     * @deprecated Instead inject game manager into component
     */
    public static GameManager manager;

    public MapSetupManager mapSetupManager;


    @Deprecated
    private static Location loc;

    public static final String SCOREBOARD_HEADER = "§8» §e§lMario Party";
    public static final String PREFIX = "§6[§l§eMP§r§6]§r ";
    public static final String PREFIX_SYSTEM = "§8» §e";
    public static final String RESOUCEPACK_URL = "http://roxhit.de/download/marioparty-rp.zip";
    public static final String TAG = "§7[ §eMP §7]§r ";
    public static final String CONFIG_ANCHOR_SECTION_PATH = "Anchors";

    @Override
    public void onEnable() {

        this.getServer().getPluginManager().registerEvents(this, this);

        Utils.init();

        manager = new GameManager(this);
        mapSetupManager = new MapSetupManager(this);
        reloadConfig();


//        if(getConfig().contains(CONFIG_ANCHOR_SECTION_PATH)){
//
//            ConfigurationSection section = getConfig().getConfigurationSection(CONFIG_ANCHOR_SECTION_PATH);
//            fieldSize = 0;
//            locations.clear();
//            for(String key : section.getKeys(false)){
//                int i = Integer.parseInt(key);
//                String path = CONFIG_ANCHOR_SECTION_PATH + "." + key + ".";
//
//                PathLocation loc = new PathLocation(Bukkit.getWorld(getConfig().getString(path + "world"))
//                        , getConfig().getDouble(path + "x")
//                        , getConfig().getDouble(path + "y")
//                        , getConfig().getDouble(path + "z")
//                        , getConfig().getDouble(path + "multiply")
//                        , getConfig().getDouble(path + "addY")
//                        , FieldType.NONE);
//                fieldSize++;
//                locations.put(fieldSize, loc);
//            }
//        }

        MapModuleManager.setOwner(this);

        this.getCommand("mapmodule").setExecutor(((commandSender, command, s, args) -> {
            MapModuleManager.getInstance().getById(args[0]).run(manager.getPlayer((commandSender).getName()));
            System.out.println("Module Started!");
            return true;
        }));
//        this.getCommand("addAnchor").setExecutor((commandSender, command, s, args) -> {
//            Player p = (Player)commandSender;
//            fieldSize++;
//            locations.put(fieldSize, new PathLocation(p.getLocation(), Double.parseDouble(args[0]), Double.parseDouble(args[1]), FieldType.valueOf(args[2].toUpperCase())));
//            commandSender.sendMessage("Punkt hinzugefügt");
//            return true;
//        });
        this.getCommand("addpowerup").setExecutor((commandSender, command, s, strings) -> {
            ArmorStand armorStand = (ArmorStand) ((Player)commandSender).getWorld().spawnEntity(((Player)commandSender).getLocation(), EntityType.ARMOR_STAND);
            armorStand.setVisible(false);
            armorStand.setSilent(true);
            armorStand.setGravity(false);
            armorStand.setCustomNameVisible(true);
            armorStand.setCustomName("§5§lPowerUp!!!");
            armorStand.setHelmet(new CustomHead("PowerUp", Utils.BASE_64_POWERUP, Category.ALPHABET).getSkull());
            new BukkitRunnable() {
                @Override
                public void run() {
                    Location loc = armorStand.getLocation();
                    loc.setYaw(loc.getYaw() + 10);
                    armorStand.teleport(loc);
                }
            }.runTaskTimer(this, 0, 5);
            return true;
        });
        this.getCommand("moveP").setExecutor((commandSender, command, s, args) -> {


            final Player player = (Player)commandSender;

            GamePlayer p = new GamePlayer(player, this);

            p.controller.moveAlong(manager.map.getAnchors(Integer.parseInt(args[0]), Integer.parseInt(args[1]))).then(() -> System.out.println("Finished"));

            System.out.println(player.getWalkSpeed());
            player.setWalkSpeed(0);
            commandSender.sendMessage("Spieler wird bewegt");

            //movePlayer(p, (ArrayList<PathLocation>) locations.clone(), 0);

            return true;
        });
//        this.getCommand("flush").setExecutor((commandSender, command, s, strings) -> {
//            locations.clear();
//            return true;
//        });
        this.getCommand("coin").setExecutor((commandSender, command, s, args) -> {
            Player p = (Player)commandSender;
            manager.summonCoin(new GamePlayer(p, this), Integer.parseInt(args[0]));
            return true;
        });

        this.getCommand("setup").setExecutor(((commandSender, command, s, args) -> {

            if(args.length >= 1){
                if(WorldConfiguration.configExists(getDataFolder(), args[0])){
                    mapSetupManager.toSetup = new GameMap(this, args[0]);
                    commandSender.sendMessage("§3Map to set up is now " + args[0]);
                    return true;
                }else{
                    commandSender.sendMessage("§4This map has not been initialized yet");
                    return true;
                }
            }

            Player p = (Player) commandSender;

            mapSetupManager.setPlayerSetupMode(p, true);

            return true;
        }));

        this.getCommand("newMap").setExecutor(((commandSender, command, s, args) -> {

            // TODO: User feedback

            mapSetupManager.toSetup = new GameMap(this, args[0]);
            mapSetupManager.toSetup.config.saveConfig();

            commandSender.sendMessage("§3Map to set up is now " + args[0]);

            return true;
        }));
//        this.getCommand("saveconfig").setExecutor((commandSender, command, s, strings) -> {
//            for(Map.Entry<Integer, PathLocation> entry : locations.entrySet()){
//                String path = CONFIG_ANCHOR_SECTION_PATH + "." + entry.getKey() + ".";
//                PathLocation loc = entry.getValue();
//                getConfig().set(path + "world", loc.getWorld().getName());
//                getConfig().set(path + "x", loc.getBlockX() + 0.5);
//                getConfig().set(path + "y", loc.getBlockY());
//                getConfig().set(path + "z", loc.getBlockZ() + 0.5);
//                getConfig().set(path + "multiply", loc.multiply);
//                getConfig().set(path + "addY", loc.addY);
//                getConfig().set(path + "fieldType", loc.getFieldType().toString());
//            }
//            saveConfig();
//            return true;
//        });
        this.getCommand("start").setExecutor((commandSender, command, s, args) -> {

            if(args.length >= 1){
                args[0] = args[0].replace("_", " ");
                Minigame minigame = manager.minigameManager.getMinigameByName(args[0]);

                if(minigame != null){
                    manager.minigameManager.startMinigame(minigame);
                }else{
                    commandSender.sendMessage("Diese Spiel ist nicht vorhanden: ");
                    for(MinigamePlugin pl : manager.minigameManager.getMinigames()){
                        commandSender.sendMessage("- " + pl.getMinigame().getName());
                    }
                }
                return true;
            }


            manager.start();

//            startGame();
            return true;
        });
//        this.getCommand("showanchor").setExecutor((commandSender, command, s, strings) -> {
//            if(displayAnchorTask == null){
//                displayAnchorTask = new BukkitRunnable() {
//                    @Override
//                    public void run() {
//                        for(Anchor loc : map.getAllAnchors().values()){
//                            loc.getWorld().spawnParticle(Particle.BARRIER, loc.clone().add(0, 1, 0), 10);
//                        }
//                    }
//                };
//            }
//            displayAnchorTask.runTaskTimer(this, 0, 20);
//            return true;
//        });
//        this.getCommand("hideanchor").setExecutor((commandSender, command, s, strings) -> {
//            displayAnchorTask.cancel();
//            return true;
//        });
        this.getCommand("join").setExecutor((commandSender, command, s, strings) -> {
            if(!(commandSender instanceof Player))
                return true;
            manager.join((Player) commandSender);
            return true;
        });
        this.getCommand("quit").setExecutor((commandSender, command, s, strings) -> {
            if(!(commandSender instanceof Player))
                return true;
            manager.quit((Player) commandSender);
            return true;
        });
        this.getCommand("resourcepack").setExecutor((commandSender, command, s, args) -> {
            ((Player)commandSender).setResourcePack(RESOUCEPACK_URL);
            return true;
        });
        this.getCommand("games").setExecutor((commandSender, command, s, strings) -> {
            ((Player)commandSender).openInventory(Inventories.getGamesInventory());
            return true;
        });
        this.getCommand("setmove").setExecutor(((commandSender, command, s, args) -> {
            manager.getPlayer(commandSender.getName()).setMoveEnabled(Boolean.parseBoolean(args[0]));
            return true;
        }));
        this.getCommand("resetplayer").setExecutor((commandSender, command, s, strings) -> {
            Player p = (Player) commandSender;

            p.setWalkSpeed(0.2f);
            p.removePotionEffect(PotionEffectType.JUMP);

            return true;
        });
        this.getCommand("music").setExecutor(((commandSender, command, s, args) -> {
            GamePlayer p = manager.getPlayer(commandSender.getName());

            if(manager.musicManager.isRegistered(p)){
                manager.musicManager.unregisterPlayer(p);
                p.getPlayer().sendMessage(PREFIX_SYSTEM + "Musik ist nun aus. Du kannst die Musik mit /music wieder starten");
            }else{
                manager.musicManager.registerPlayer(p);
                manager.musicManager.startMusic(p);
                p.getPlayer().sendMessage(PREFIX_SYSTEM + "Musik ist nun an. Du kannst die Musik mit /music wieder stoppen");
            }


            return true;
        }));


        /*for(int i = 0; i < 20; i++){
            summonPowerUp(this);
        }*/
        super.onEnable();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        Player p = e.getPlayer();
        e.setJoinMessage(PREFIX_SYSTEM + e.getPlayer().getName() + " hat das Spiel betreten!");


        //p.setResourcePack(RESOUCEPACK_URL);
    }


    @EventHandler
    public void onPlayerResourceEvent(PlayerResourcePackStatusEvent e){
        System.out.println(e.getStatus().name());
    }


    @Override
    public void onDisable() {
        if(manager != null)
        manager.disable();

        mapSetupManager.removeArmorStands();

    }


    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e){
    }



    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e){
        e.setFoodLevel(20);
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent e)
        {boolean rain = e.toWeatherState();
        if(rain)
            e.setCancelled(true);
    }
    @EventHandler
    public void onThunderChange(ThunderChangeEvent event) {

        boolean storm = event.toThunderState();
        if(storm)
            event.setCancelled(true);
    }





}


