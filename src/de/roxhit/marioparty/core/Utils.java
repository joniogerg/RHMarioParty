package de.roxhit.marioparty.core;

import de.roxhit.heads.main.CustomHead;
import de.walamana.fb64s.main.Category;
import org.apache.logging.log4j.core.pattern.AbstractStyleNameConverter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by jonio on 14.06.2017.
 */
public class Utils {

    public static final String BASE_64_DICE_1 = "eyJ0aW1lc3RhbXAiOjE1MDM1ODI0NTg2NTQsInByb2ZpbGVJZCI6IjNiZTZjMDFlOGY2OTRkNGU4ZGEwMjg2NDk5MTU4ZWM1IiwicHJvZmlsZU5hbWUiOiJ3YWxhbWFuYSIsInRleHR1cmVzIjp7IlNLSU4iOnsibWV0YWRhdGEiOnsibW9kZWwiOiJzbGltIn0sInVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZGQyZGMzYTdhZjdmM2U1ZWE3OTdiNTc0Mzc4YjE3NDhkN2ZkYzJmM2Q2ZWVlMWU2ZjVmNWQzNTNmNjQyMjMifX19";
    public static final String BASE_64_DICE_2 = "eyJ0aW1lc3RhbXAiOjE1MDM1ODI1NTAwMDIsInByb2ZpbGVJZCI6IjNiZTZjMDFlOGY2OTRkNGU4ZGEwMjg2NDk5MTU4ZWM1IiwicHJvZmlsZU5hbWUiOiJ3YWxhbWFuYSIsInRleHR1cmVzIjp7IlNLSU4iOnsibWV0YWRhdGEiOnsibW9kZWwiOiJzbGltIn0sInVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODkxOThmNzVjOGIwMTY1YTk4MjBiZGY2YTdkNmM1NTlkNDQ0ZmQyNGU4NDJkZjYzMDZlMWZjNzE1MTNlNGEifX19";
    public static final String BASE_64_DICE_3 = "eyJ0aW1lc3RhbXAiOjE1MDM1ODI2MTg2NDQsInByb2ZpbGVJZCI6IjNiZTZjMDFlOGY2OTRkNGU4ZGEwMjg2NDk5MTU4ZWM1IiwicHJvZmlsZU5hbWUiOiJ3YWxhbWFuYSIsInRleHR1cmVzIjp7IlNLSU4iOnsibWV0YWRhdGEiOnsibW9kZWwiOiJzbGltIn0sInVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzU5NTkzMzFlMWNiNTUxMTc2MWNlYjE3ZDgxYzZkNjVmMzJkZTUzNzI5YjIzZTcxZTNkNzgwYTFkMGI0In19fQ==";
    public static final String BASE_64_DICE_4 = "eyJ0aW1lc3RhbXAiOjE1MDM1ODI2NDk2MDgsInByb2ZpbGVJZCI6IjNiZTZjMDFlOGY2OTRkNGU4ZGEwMjg2NDk5MTU4ZWM1IiwicHJvZmlsZU5hbWUiOiJ3YWxhbWFuYSIsInRleHR1cmVzIjp7IlNLSU4iOnsibWV0YWRhdGEiOnsibW9kZWwiOiJzbGltIn0sInVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzY3ZjQzZmNhNWFjNDRhYTQ0NzVhZWIxNWU5YzBkY2ZkNDhiNTdhNTVmNTZkZDA3NzNlMzA5ODBkNyJ9fX0=";
    public static final String BASE_64_DICE_5 = "eyJ0aW1lc3RhbXAiOjE1MDM1ODI2ODIwNjksInByb2ZpbGVJZCI6IjNiZTZjMDFlOGY2OTRkNGU4ZGEwMjg2NDk5MTU4ZWM1IiwicHJvZmlsZU5hbWUiOiJ3YWxhbWFuYSIsInRleHR1cmVzIjp7IlNLSU4iOnsibWV0YWRhdGEiOnsibW9kZWwiOiJzbGltIn0sInVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTFjYmVhYjk0NDI4YmM5OTQ0ZWNiY2E3NzJlNzBmNDNmNzJlNDM4M2NhOTE5YmY5ZjgyZjZjZmZiYjU2OTcifX19";
    public static final String BASE_64_DICE_6 = "eyJ0aW1lc3RhbXAiOjE1MDM1ODI3MzA3ODksInByb2ZpbGVJZCI6IjNiZTZjMDFlOGY2OTRkNGU4ZGEwMjg2NDk5MTU4ZWM1IiwicHJvZmlsZU5hbWUiOiJ3YWxhbWFuYSIsInRleHR1cmVzIjp7IlNLSU4iOnsibWV0YWRhdGEiOnsibW9kZWwiOiJzbGltIn0sInVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjdhNTE5ZGZiYTJmYzE4OTU1MTQyOWI5OTM2YTkwNmE2NWVjN2Q0NGU3OWNiNDE1YTNkZjhiMzlmMTdlN2E3NiJ9fX0=";
    public static final String BASE_64_DICE =   "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzk3OTU1NDYyZTRlNTc2NjY0NDk5YWM0YTFjNTcyZjYxNDNmMTlhZDJkNjE5NDc3NjE5OGY4ZDEzNmZkYjIifX19";
    public static final String BASE_64_COIN = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjZkMWNlNjk3ZTlkYmFhNGNjZjY0MjUxNmFhYTU5ODEzMzJkYWMxZDMzMWFmZWUyZWUzZGNjODllZmRlZGIifX19";
    public static final String BASE_64_SNAPBACK = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzRlY2MwNDA3ODVlNTQ2NjNlODU1ZWYwNDg2ZGE3MjE1NGQ2OWJiNGI3NDI0YjczODFjY2Y5NWIwOTVhIn19fQ==";
    public static final String BASE_64_POWERUP = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWQ5Y2M1OGFkMjVhMWFiMTZkMzZiYjVkNmQ0OTNjOGY1ODk4YzJiZjMwMmI2NGUzMjU5MjFjNDFjMzU4NjcifX19";
    public static final String BASE_64_RED = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTdjMWYxZWFkNGQ1MzFjYWE0YTViMGQ2OWVkYmNlMjlhZjc4OWEyNTUwZTVkZGJkMjM3NzViZTA1ZTJkZjJjNCJ9fX0=";
    public static final String BASE_64_YELLOW = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTRjNDE0MWMxZWRmM2Y3ZTQxMjM2YmQ2NThjNWJjN2I1YWE3YWJmN2UyYTg1MmI2NDcyNTg4MThhY2Q3MGQ4In19fQ==";
    public static final String BASE_64_GREEN = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzZmNjlmN2I3NTM4YjQxZGMzNDM5ZjM2NThhYmJkNTlmYWNjYTM2NmYxOTBiY2YxZDZkMGEwMjZjOGY5NiJ9fX0=";
    private static final Random random = new Random();

    public static final ArrayList<ItemStack> powerUpItems = new ArrayList<>();
    public static final ArrayList<ItemStack> bowserItems = new ArrayList<>();
    public static ItemStack score = null;

    public static void init(){
        powerUpItems.clear();
        ArrayList<ItemStack> items = new ArrayList<>();

        ItemStack dice = new CustomHead("Dice", BASE_64_DICE, Category.ALPHABET ).getSkull();
        ItemMeta diceMeta = dice.getItemMeta();
        diceMeta.setDisplayName("Dice");
        dice.setItemMeta(diceMeta);

        ItemStack coin = new CustomHead("Dice", BASE_64_COIN, Category.ALPHABET ).getSkull();
        ItemMeta coinMeta = coin.getItemMeta();
        coinMeta.setDisplayName("Coin");
        coin.setItemMeta(coinMeta);

        ItemStack snapback = new CustomHead("Snapback", BASE_64_SNAPBACK, Category.ALPHABET ).getSkull();
        ItemMeta snapbackMeta = snapback.getItemMeta();
        snapbackMeta.setDisplayName("Snapback");
        snapback.setItemMeta(snapbackMeta);

        items.add(dice);
        items.add(coin);
        items.add(snapback);

        ItemStack red = new CustomHead("§4Hälfte der Münzen weg!" , BASE_64_RED, Category.COLOR).getSkull();
        ItemMeta metaR = red.getItemMeta();
        metaR.setLore(Arrays.asList("red"));
        red.setItemMeta(metaR);

        ItemStack yellow = new CustomHead("§eNichts" , BASE_64_YELLOW, Category.COLOR).getSkull();
        ItemMeta metaY = yellow.getItemMeta();
        metaY.setLore(Arrays.asList("yellow"));
        yellow.setItemMeta(metaY);

        ItemStack green = new CustomHead("§e+5 Münzen!" , BASE_64_GREEN, Category.COLOR).getSkull();
        ItemMeta metaG = green.getItemMeta();
        metaG.setLore(Arrays.asList("green"));
        green.setItemMeta(metaG);

        for(int i = 0; i < 5; i++){
            bowserItems.add(red);
        }
        for(int i = 0; i < 3; i++){
            bowserItems.add(yellow);
        }
        for(int i = 0; i < 2; i++){
            bowserItems.add(green);
        }



        score = new ItemStack(Material.WATCH);
        ItemMeta metaScore = score.getItemMeta();
        metaScore.setDisplayName("§3Spielstand");
        score.setItemMeta(metaScore);

        powerUpItems.addAll(items);
    }

    public static String getDiceResource(int num) {
        switch (num) {
            case 1:
                return BASE_64_DICE_1;
            case 2:
                return BASE_64_DICE_2;
            case 3:
                return BASE_64_DICE_3;
            case 4:
                return BASE_64_DICE_4;
            case 5:
                return BASE_64_DICE_5;
            case 6:
                return BASE_64_DICE_6;
        }
        return "";
    }

    public static int getRandomNumber(int min, int max){
        return random.nextInt(max - min) + min;
    }
    public static double getRandomDouble(double min, double max){
        return (random.nextDouble() * max )+ min ;
    }


    public static ArrayList<String> cutString(String str){
        return cutString(str, ChatColor.WHITE);
    }

    public static ArrayList<String> cutString(String str, ChatColor color){
        ArrayList<String> strings = new ArrayList<>();
        String[] items = str.split(" ");
        StringBuilder tmp = new StringBuilder();
        int counter = 0;

        for(String s : items){
            counter += s.length();
            tmp.append(" ").append(color).append(s);
            if(counter >= 30){
                strings.add(tmp.toString());
                tmp = new StringBuilder();
                counter = 0;
            }
        }
        if(!tmp.toString().equalsIgnoreCase(""))
        strings.add(tmp.toString());

        return strings;
    }

    public static boolean isLoaded(String worldName){
        for(World world : Bukkit.getWorlds()){
            if(world.getName().equalsIgnoreCase(worldName)){
                return true;
            }
        }
        return false;
    }




}
