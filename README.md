# Bukkit 1.12 - Mario Party API

##  THERE WILL BE A MAJOR CHANGE IN THE FIELD AND ANCHOR SYSTEM

**THIS PLUGIN IS STILL IN DEVELOPMENT**

_**THERE MIGHT BE SOME MINOR (MAY MAJOR) API CHANGES!!!**_

Play minigames like in the Nintendos classic Mario Party

Features like:
- Game API
- Circular Map
- Custom map modules


### Missing features:
- Game setup
- Minigame list
- Translation

### What are map modules?
You can create your own map specific map modules. They can run special actions like a Tod house or something like that.
 
 _**Be creative**_.
 
 
I've added you a example for a map module. If you want to try it please contact me for instructions

### Where do I get minigames from?
Here is a small list of minigames I made. Since I created them while developing the API they might be buggy at some point. They are also not configurable yet, so you have to figure out where the important locations are.
_The source code will be published later..._

- https://gitlab.com/joniogerg/RHMarioParty/tree/master/minigames

### How to use?

  _Documentation is being added_
  
  
### Where do I get the dependencies from?

The RHVector dependency is being included into the project itself. Please wait.

This plugin uses the NoteBlockAPI from koca2000 (https://www.spigotmc.org/resources/noteblockapi.19287/)
LICENCE: https://github.com/koca2000/NoteBlockAPI/blob/master/LICENSE
