package de.roxhit.marioparty.core.interfaces;

/**
 * Created by jonio on 27.06.2017.
 */
public interface MinigamePlugin {

    Minigame getMinigame();
}
