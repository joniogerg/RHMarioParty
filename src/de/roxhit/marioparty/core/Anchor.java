package de.roxhit.marioparty.core;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;

/**
 * Created by jonio on 24.08.2017.
 */
public class Anchor extends Location {

    public double multiply, addY;
    private boolean secondaryAnchor;
    private ArmorStand powerupEntity = null;
    private int ID;

    public Anchor(int id, Location loc, double multiply, double addY, boolean secondaryAnchor){
        super(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ());
        ID = id;
        this.multiply = multiply;
        this.addY = addY;
        this.secondaryAnchor = secondaryAnchor;
    }

    public Anchor(int id, World world, double x, double y, double z, double multiply, double addY, boolean secondaryAnchor) {
        super(world, x, y, z);
        ID = id;
        this.multiply = multiply;
        this.addY = addY;
        this.secondaryAnchor = secondaryAnchor;
    }

    public int getID() {
        return ID;
    }

    public FieldLocation getFieldLocation(GameMap map){
        for(FieldLocation loc : map.fields){
            if(loc.getFinalAnchor() == this){
                return loc;
            }
        }
        return null;
    }

    public boolean isSecondaryAnchor() {
        return secondaryAnchor;
    }

    public void setIsSecondaryAnchor(boolean isSecondaryAnchor) {
        this.secondaryAnchor = isSecondaryAnchor;
    }


    public ArmorStand getPowerupEntity() {
        return powerupEntity;
    }

    public void setPowerupEntity(ArmorStand powerupEntity) {
        this.powerupEntity = powerupEntity;
    }

    @Override
    public String toString() {
        return "Anchor{" +
                super.toString() +
                ", multiply=" + multiply +
                ", addY=" + addY +
                ", secondaryAnchor=" + secondaryAnchor +
                ", powerupEntity=" + powerupEntity +
                ", ID=" + ID +
                '}';
    }
}
