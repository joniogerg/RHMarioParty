package de.roxhit.marioparty.core.interfaces;

import de.roxhit.marioparty.core.GamePlayer;
import de.roxhit.marioparty.core.Result;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jonio on 26.06.2017.
 */
public interface Minigame {

    void startGame();
    void unregisterListener();

    ArrayList<GamePlayer> getGamePlayer();
    ArrayList<Location> getSpawnLocations();
    BukkitRunnable getGameTask();
    Plugin getOwner();
    String getDescription();
    String getName();
    Result getResults();
    ItemStack getIcon();


}
